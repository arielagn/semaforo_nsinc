
/*
 * File:   ControlSem.c
 * Author: LPT13
 *
 * 
 */
#include "mcc_generated_files/adc1.h"
#include "mcc_generated_files/pin_manager.h"
#include "ControlSem.h"
#include "PCA9555.h"

uint8_t analogTaskSt,port0, port1;
extern Sem_Conf semConf;

void analogTask()
{
    
    switch (analogTaskSt)
    {
        case 0:
            port0=i9555_read(0);
            port1=i9555_read(1);
            analogTaskSt++;
            break;
        case 1:
            ADC1_ChannelSelect( ADC1_LINEA );
            ADC1_Start();
            analogTaskSt++;
            break;
        case 2:
            if (ADC1_IsConversionComplete())
            {
                semConf.V_linea=ADC1_ConversionResultGet();
                analogTaskSt++;
            }
            break;
        case 3:
            ADC1_ChannelSelect( ADC1_RET1 );
            ADC1_Start();
            analogTaskSt++;
            break;
        case 4:
            if (ADC1_IsConversionComplete())
            {
                semConf.ret1=ADC1_ConversionResultGet();
                analogTaskSt++;
            }
            break;
        case 5:
            ADC1_ChannelSelect( ADC1_RET2 );
            ADC1_Start();
            analogTaskSt++;
            break;
        case 6:
            if (ADC1_IsConversionComplete())
            {
                semConf.ret2=ADC1_ConversionResultGet();
                analogTaskSt++;
            }
            break;
        case 7:
            ADC1_ChannelSelect( ADC1_RET3 );
            ADC1_Start();
            analogTaskSt++;
            break;
        case 8:
            if (ADC1_IsConversionComplete())
            {
                semConf.ret3=ADC1_ConversionResultGet();
                analogTaskSt++;
            }
            break;
        case 9:
            ADC1_ChannelSelect( ADC1_RET4 );
            ADC1_Start();
            analogTaskSt++;
            break;
        case 10:
            if (ADC1_IsConversionComplete())
            {
                semConf.ret4=ADC1_ConversionResultGet();
                analogTaskSt=1;
                //LED2_Toggle();
            }
            break;
        default:
            break;
    }
    ADC1_Tasks ();
}
void setAnalogTaskSt(uint8_t sts)
{
    analogTaskSt=sts;
}
uint8_t getAnalogTaskSt()
{
    return (analogTaskSt);
}
/* *****************************************************************************
 End of File
 */
