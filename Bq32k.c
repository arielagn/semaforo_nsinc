/*
 * File:   Bq32k.c
 * Author: LPT13
 *
 * Created on November 19, 2017, 7:45 PM
 */


#include "xc.h"
#include "Bq32k.h"
#include "mcc_generated_files/i2c1.h"
#include "ControlSem.h"
//static bq32k RTCC_S;
extern Sem_Conf semConf;


uint8_t BQ32k_init()
{     
   I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
   uint8_t     writeBuffer[4];
   uint16_t    retryTimeOut, slaveTimeOut;
   writeBuffer[0]=BQ32K_CAL_CFG1;
   writeBuffer[1]=0x40;
   writeBuffer[2]=0x00;
   writeBuffer[3]=0x00;
   
   retryTimeOut = 0;
   slaveTimeOut = 0;
   
    while(status != I2C1_MESSAGE_FAIL)
    {
        I2C1_MasterWrite(    writeBuffer,
                                4,
                                i2c_w,
                                &status);
        while(status == I2C1_MESSAGE_PENDING)
        {

            if (slaveTimeOut == BQ32K_DEVICE_TIMEOUT)
                return (0);
            else
                slaveTimeOut++;
        }

        if (status == I2C1_MESSAGE_COMPLETE)
            break;


        if (retryTimeOut == BQ32K_RETRY_MAX)
            return(0);
        else
            retryTimeOut++;
    }
    writeBuffer[0]=BQ32K_SF_KEY1;
    writeBuffer[1]=0x5e;
    writeBuffer[2]=0xc7;
    writeBuffer[3]=0x01;
   
    retryTimeOut = 0;
    slaveTimeOut = 0;
   
    while(status != I2C1_MESSAGE_FAIL)
    {
        I2C1_MasterWrite(    writeBuffer,
                                4,
                                i2c_w,
                                &status);
        while(status == I2C1_MESSAGE_PENDING)
        {

            if (slaveTimeOut == BQ32K_DEVICE_TIMEOUT)
                return (0);
            else
                slaveTimeOut++;
        }

        if (status == I2C1_MESSAGE_COMPLETE)
            break;


        if (retryTimeOut == BQ32K_RETRY_MAX)
            return(0);
        else
            retryTimeOut++;
    }
    return(1);
}

uint8_t BQ32k_set_DateTime()
{   
    if (semConf.actualizar==0)
        return(2);  //Nada que hacer
    // Actualizo Dia
    I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
    uint8_t     writeBuffer[4];
    uint16_t    retryTimeOut, slaveTimeOut;
    uint8_t day, mth, year;
    day =semConf.date & 0x3F;
    mth =semConf.meses & 0x1F;
    year=semConf.anios & 0xFF;
   
    writeBuffer[0]=BQ32K_DATE;
    writeBuffer[1]=bin2bcd(day);
    writeBuffer[2]=bin2bcd(mth);
    writeBuffer[3]=bin2bcd(year);
   
    retryTimeOut = 0;
    slaveTimeOut = 0;
   
    while(status != I2C1_MESSAGE_FAIL)
    {
        I2C1_MasterWrite(    writeBuffer,
                                4,
                                i2c_w,
                                &status);
        while(status == I2C1_MESSAGE_PENDING)
        {

            if (slaveTimeOut == BQ32K_DEVICE_TIMEOUT)
                return (0);
            else
                slaveTimeOut++;
        }

        if (status == I2C1_MESSAGE_COMPLETE)
            break;


        if (retryTimeOut == BQ32K_RETRY_MAX)
            return(0);
        else
            retryTimeOut++;
    }
    //// Actualizo hora
    status = I2C1_MESSAGE_PENDING; //I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
    //uint8_t     writeBuffer[4];
    //uint16_t    retryTimeOut, slaveTimeOut;
    uint8_t hrs,min,seg;
    seg=  semConf.segundos & 0x7F;
    min=  semConf.minutos & 0x7F;
    hrs=  semConf.horas & 0x3F;
    writeBuffer[0]=BQ32K_SECONDS;
    writeBuffer[1]=bin2bcd(seg);
    writeBuffer[2]=bin2bcd(min);
    writeBuffer[3]=bin2bcd(hrs);
   
    retryTimeOut = 0;
    slaveTimeOut = 0;
   
    while(status != I2C1_MESSAGE_FAIL)
    {
        I2C1_MasterWrite(    writeBuffer,
                                4,
                                i2c_w,
                                &status);
        while(status == I2C1_MESSAGE_PENDING)
        {

            if (slaveTimeOut == BQ32K_DEVICE_TIMEOUT)
                return (0);
            else
                slaveTimeOut++;
        }

        if (status == I2C1_MESSAGE_COMPLETE)
            break;


        if (retryTimeOut == BQ32K_RETRY_MAX)
            return(0);
        else
            retryTimeOut++;
    }
    semConf.actualizar=0; //Todo actualizado
    return(1);
}

uint8_t BQ32k_get_date()
{   I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
    uint8_t     writeBuffer[4];
    uint8_t     readBuffer[4];
    uint16_t    retryTimeOut, slaveTimeOut;
    writeBuffer[0]=BQ32K_DAY;
      
    retryTimeOut = 0;
    slaveTimeOut = 0;
   
    while(status != I2C1_MESSAGE_FAIL)
    {
        I2C1_MasterWrite(    writeBuffer,
                                1,
                                i2c_w,
                                &status);
        while(status == I2C1_MESSAGE_PENDING)
        {

            if (slaveTimeOut == BQ32K_DEVICE_TIMEOUT)
                return (0);
            else
                slaveTimeOut++;
        }

        if (status == I2C1_MESSAGE_COMPLETE)
            break;


        if (retryTimeOut == BQ32K_RETRY_MAX)
            return(0);
        else
            retryTimeOut++;
    }
    if (status == I2C1_MESSAGE_COMPLETE)
    {

        retryTimeOut = 0;
        slaveTimeOut = 0;

        while(status != I2C1_MESSAGE_FAIL)
        {
            I2C1_MasterRead(     readBuffer,
                            4,
                            i2c_w,
                            &status);

            while(status == I2C1_MESSAGE_PENDING)
            {
                if (slaveTimeOut == BQ32K_DEVICE_TIMEOUT)
                    return (0);
                else
                    slaveTimeOut++;
            }

            if (status == I2C1_MESSAGE_COMPLETE)
                break;

            if (retryTimeOut == BQ32K_RETRY_MAX)
                return(0);
            else
                retryTimeOut++;
        }
        // exit if the last transaction failed
        if (status == I2C1_MESSAGE_FAIL)
            return(0);
        
        semConf.dia = bcd2bin(readBuffer[0] & 0x7f);   // REG 3
        semConf.date  = bcd2bin(readBuffer[1] & 0x3f);   // REG 4
        semConf.meses  = bcd2bin(readBuffer[2] & 0x1f);   // REG 5
        semConf.anios = bcd2bin(readBuffer[3]);            // REG 6
        return(1);
    }
    return(0);
} 

uint8_t BQ32k_get_time()
{   I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
    uint8_t     writeBuffer[4];
    uint8_t     readBuffer[4];
    uint16_t    retryTimeOut, slaveTimeOut;
    writeBuffer[0]=BQ32K_SECONDS;
      
    retryTimeOut = 0;
    slaveTimeOut = 0;
   
    while(status != I2C1_MESSAGE_FAIL)
    {
        I2C1_MasterWrite(    writeBuffer,
                                1,
                                i2c_w,
                                &status);
        while(status == I2C1_MESSAGE_PENDING)
        {

            if (slaveTimeOut == BQ32K_DEVICE_TIMEOUT)
                return (0);
            else
                slaveTimeOut++;
        }

        if (status == I2C1_MESSAGE_COMPLETE)
            break;


        if (retryTimeOut == BQ32K_RETRY_MAX)
            return(0);
        else
            retryTimeOut++;
    }
    if (status == I2C1_MESSAGE_COMPLETE)
    {

        retryTimeOut = 0;
        slaveTimeOut = 0;

        while(status != I2C1_MESSAGE_FAIL)
        {
            I2C1_MasterRead(     readBuffer,
                            3,
                            i2c_w,
                            &status);

            while(status == I2C1_MESSAGE_PENDING)
            {
                if (slaveTimeOut == BQ32K_DEVICE_TIMEOUT)
                    return (0);
                else
                    slaveTimeOut++;
            }

            if (status == I2C1_MESSAGE_COMPLETE)
                break;

            if (retryTimeOut == BQ32K_RETRY_MAX)
                return(0);
            else
                retryTimeOut++;
        }
        // exit if the last transaction failed
        if (status == I2C1_MESSAGE_FAIL)
            return(0);
           
        semConf.segundos = bcd2bin(readBuffer[0] & 0x7f);   
        semConf.minutos  = bcd2bin(readBuffer[1] & 0x7f);  
        semConf.horas  = bcd2bin(readBuffer[2] & 0x3f);   
        
        return(1);
    }
    return(0);    
} 

uint8_t bin2bcd(uint8_t binary_value) 
{ 
  uint8_t temp; 
  uint8_t retval; 

  temp = binary_value; 
  retval = 0; 

  while(1) 
  { 
    // Get the tens digit by doing multiple subtraction 
    // of 10 from the binary value. 
    if(temp >= 10) 
    { 
      temp -= 10; 
      retval += 0x10; 
    } 
    else // Get the ones digit by adding the remainder. 
    { 
      retval += temp; 
      break; 
    } 
  } 

  return(retval); 
} 

// Input range - 00 to 99. 
uint8_t bcd2bin(uint8_t bcd_value)
{ 
 uint8_t temp; 

  temp = bcd_value; 
  // Shifting upper digit right by 1 is same as multiplying by 8. 
  temp >>= 1; 
  // Isolate the bits for the upper digit. 
  temp &= 0x78; 

  // Now return: (Tens * 8) + (Tens * 2) + Ones 

  return(temp + (temp >> 2) + (bcd_value & 0x0f));
  
    /*return (((bcd_value >> 4)*10)+(bcd_value & 0x0f)); */

}
