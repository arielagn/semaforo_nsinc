/*
 * File:   PCA9555.c
 * Author: LPT13
 *
 * Created on November 20, 2017, 4:10 PM
 */


#include "xc.h"
#include "PCA9555.h"
#include "mcc_generated_files/i2c2.h"

uint8_t i9555_Init()
{
    I2C2_MESSAGE_STATUS status = I2C2_MESSAGE_PENDING;
    uint8_t     writeBuffer[4];
    uint16_t    retryTimeOut, slaveTimeOut;
    writeBuffer[0]=i9555_reg_confP0;
    writeBuffer[1]=0b00000011;
    writeBuffer[2]=0b01000000;
   
    retryTimeOut = 0;
    slaveTimeOut = 0;
   
    while(status != I2C2_MESSAGE_FAIL)
    {
        I2C2_MasterWrite(    writeBuffer,
                                3,
                                i9555_i2c_addr_wr,
                                &status);
        while(status == I2C2_MESSAGE_PENDING)
        {

            if (slaveTimeOut == i9555_DEVICE_TIMEOUT)
                return (0);
            else
                slaveTimeOut++;
        }

        if (status == I2C2_MESSAGE_COMPLETE)
            break;


        if (retryTimeOut == i9555_RETRY_MAX)
            return(0);
        else
            retryTimeOut++;
    }
    writeBuffer[0]=i9555_reg_polP0;
    writeBuffer[1]=0x00;
    writeBuffer[2]=0x00;
   
    retryTimeOut = 0;
    slaveTimeOut = 0;
   
    while(status != I2C2_MESSAGE_FAIL)
    {
        I2C2_MasterWrite(    writeBuffer,
                                3,
                                i9555_i2c_addr_wr,
                                &status);
        while(status == I2C2_MESSAGE_PENDING)
        {

            if (slaveTimeOut == i9555_DEVICE_TIMEOUT)
                return (0);
            else
                slaveTimeOut++;
        }

        if (status == I2C2_MESSAGE_COMPLETE)
            break;


        if (retryTimeOut == i9555_RETRY_MAX)
            return(0);
        else
            retryTimeOut++;
    }
    i9555_write(0,0);   //Para apagar lamparas al inicio
    i9555_write(1,0);
    
    return(1);

}
 
uint8_t i9555_read(char puerto)
{
    I2C2_MESSAGE_STATUS status = I2C2_MESSAGE_PENDING;
    uint8_t     writeBuffer[2];
    uint8_t     readBuffer[2];
    uint16_t    retryTimeOut, slaveTimeOut;
    if(puerto==0)
            writeBuffer[0]=i9555_reg_inP0;
    if(puerto==1)
            writeBuffer[0]=i9555_reg_inP1;
   
    retryTimeOut = 0;
    slaveTimeOut = 0;
   
    while(status != I2C2_MESSAGE_FAIL)
    {
        I2C2_MasterWrite(    writeBuffer,
                                1,
                                i9555_i2c_addr_wr,
                                &status);
        while(status == I2C2_MESSAGE_PENDING)
        {

            if (slaveTimeOut == i9555_DEVICE_TIMEOUT)
                return (100);
            else
                slaveTimeOut++;
        }

        if (status == I2C2_MESSAGE_COMPLETE)
            break;


        if (retryTimeOut == i9555_RETRY_MAX)
            return(101);
        else
            retryTimeOut++;
    }
    if (status == I2C2_MESSAGE_COMPLETE)
    {

        retryTimeOut = 0;
        slaveTimeOut = 0;

        while(status != I2C2_MESSAGE_FAIL)
        {
            I2C2_MasterRead(     readBuffer,
                            1,
                            i9555_i2c_addr_wr,
                            &status);

            while(status == I2C2_MESSAGE_PENDING)
            {
                if (slaveTimeOut == i9555_DEVICE_TIMEOUT)
                    return (102);
                else
                    slaveTimeOut++;
            }

            if (status == I2C2_MESSAGE_COMPLETE)
                break;

            if (retryTimeOut == i9555_RETRY_MAX)
                return(103);
            else
                retryTimeOut++;
        }
        // exit if the last transaction failed
        if (status == I2C2_MESSAGE_FAIL)
            return(104);
        
        return(readBuffer[0]);
    }
    return(105);
}

uint8_t i9555_write(char puerto,char valor)
{
    I2C2_MESSAGE_STATUS status = I2C2_MESSAGE_PENDING;
    uint8_t     writeBuffer[3];
    uint16_t    retryTimeOut, slaveTimeOut;
    
    if(puerto==0)
            writeBuffer[0]=i9555_reg_outP0;
    if(puerto==1)
            writeBuffer[0]=i9555_reg_outP1;
    writeBuffer[1]=valor;
    
   
    retryTimeOut = 0;
    slaveTimeOut = 0;
   
    while(status != I2C2_MESSAGE_FAIL)
    {
        I2C2_MasterWrite(    writeBuffer,
                                2,
                                i9555_i2c_addr_wr,
                                &status);
        while(status == I2C2_MESSAGE_PENDING)
        {

            if (slaveTimeOut == i9555_DEVICE_TIMEOUT)
                return (0);
            else
                slaveTimeOut++;
        }

        if (status == I2C2_MESSAGE_COMPLETE)
            break;


        if (retryTimeOut == i9555_RETRY_MAX)
            return(0);
        else
            retryTimeOut++;
    }
    return(1);
}