/*
 * File:   24LC512.c
 * Author: LPT13
 *
 * Created on November 17, 2017, 5:02 PM
 */


#include "xc.h"
#include "A24LC512.h"
#include "ControlSem.h"
#include "mcc_generated_files/i2c1.h"

extern Sem_Conf semConf;

#define MCHP24AA512_RETRY_MAX       100  // define the retry count
#define MCHP24AA512_ADDRESS         0x51 // slave device address
#define MCHP24AA512_DEVICE_TIMEOUT  500   // define slave timeout 
#define EEPROM_SIZE   65479  //Bytes le saco un par de registro por seguridad

uint8_t MCHP24AA512_Read(
                                uint16_t address,
                                uint8_t *pData,
                                uint16_t nCount)
{
    I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
    uint8_t     writeBuffer[3];
    uint16_t    retryTimeOut, slaveTimeOut;
    uint16_t    counter;
    uint8_t     *pD;

    pD = pData;

    for (counter = 0; counter < nCount; counter++)
    {

        // build the write buffer first
        // starting address of the EEPROM memory
        writeBuffer[0] = (address >> 8);                // high address
        writeBuffer[1] = (uint8_t)(address);            // low low address

        // Now it is possible that the slave device will be slow.
        // As a work around on these slaves, the application can
        // retry sending the transaction
        retryTimeOut = 0;
        slaveTimeOut = 0;

        while(status != I2C1_MESSAGE_FAIL)
        {
            // write one byte to EEPROM (2 is the count of bytes to write)
            I2C1_MasterWrite(    writeBuffer,
                                    2,
                                    MCHP24AA512_ADDRESS,
                                    &status);

            // wait for the message to be sent or status has changed.
            while(status == I2C1_MESSAGE_PENDING)
            {
                // add some delay here

                // timeout checking
                // check for max retry and skip this byte
                if (slaveTimeOut == MCHP24AA512_DEVICE_TIMEOUT)
                    return (0);
                else
                    slaveTimeOut++;
            }

            if (status == I2C1_MESSAGE_COMPLETE)
                break;

            // if status is  I2C1_MESSAGE_ADDRESS_NO_ACK,
            //               or I2C1_DATA_NO_ACK,
            // The device may be busy and needs more time for the last
            // write so we can retry writing the data, this is why we
            // use a while loop here

            // check for max retry and skip this byte
            if (retryTimeOut == MCHP24AA512_RETRY_MAX)
                break;
            else
                retryTimeOut++;
        }

        if (status == I2C1_MESSAGE_COMPLETE)
        {

            // this portion will read the byte from the memory location.
            retryTimeOut = 0;
            slaveTimeOut = 0;

            while(status != I2C1_MESSAGE_FAIL)
            {
                // write one byte to EEPROM (2 is the count of bytes to write)
                I2C1_MasterRead(     pD,
                                        1,
                                        MCHP24AA512_ADDRESS,
                                        &status);

                // wait for the message to be sent or status has changed.
                while(status == I2C1_MESSAGE_PENDING)
                {
                    // add some delay here

                    // timeout checking
                    // check for max retry and skip this byte
                    if (slaveTimeOut == MCHP24AA512_DEVICE_TIMEOUT)
                        return (0);
                    else
                        slaveTimeOut++;
                }

                if (status == I2C1_MESSAGE_COMPLETE)
                    break;

                // if status is  I2C1_MESSAGE_ADDRESS_NO_ACK,
                //               or I2C1_DATA_NO_ACK,
                // The device may be busy and needs more time for the last
                // write so we can retry writing the data, this is why we
                // use a while loop here

                // check for max retry and skip this byte
                if (retryTimeOut == MCHP24AA512_RETRY_MAX)
                    break;
                else
                    retryTimeOut++;
            }
        }

        // exit if the last transaction failed
        if (status == I2C1_MESSAGE_FAIL)
        {
            return(0);
            break;
        }

        pD++;
        address++;

    }
    return(1);

}


uint8_t MCHP24AA512_Write(
                                uint16_t address,
                                uint8_t *pData,
                                uint16_t nCount)
{
    I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
    uint8_t     writeBuffer[3];
    uint16_t    retryTimeOut, slaveTimeOut;
    uint16_t    counter;
    uint8_t     *pD;

    pD = pData;

    for (counter = 0; counter < nCount; counter++)
    {

        // build the write buffer first
        // starting address of the EEPROM memory
        writeBuffer[0] = (address >> 8);                // high address
        writeBuffer[1] = (uint8_t)(address);            // low low address
        
        // data to be written
        writeBuffer[2] = *pD++;
        
        // Now it is possible that the slave device will be slow.
        // As a work around on these slaves, the application can
        // retry sending the transaction
        retryTimeOut = 0;
        slaveTimeOut = 0;

        while(status != I2C1_MESSAGE_FAIL)
        {
            // write one byte to EEPROM (3 is the count of bytes to write)
            I2C1_MasterWrite(    writeBuffer,
                                    3,
                                    MCHP24AA512_ADDRESS,
                                    &status);

            // wait for the message to be sent or status has changed.
            while(status == I2C1_MESSAGE_PENDING)
            {
                // add some delay here

                // timeout checking
                // check for max retry and skip this byte
                if (slaveTimeOut == MCHP24AA512_DEVICE_TIMEOUT)
                    break;
                else
                    slaveTimeOut++;
            }

            if (status == I2C1_MESSAGE_COMPLETE)
                break;

            // if status is  I2C1_MESSAGE_ADDRESS_NO_ACK,
            //               or I2C1_DATA_NO_ACK,
            // The device may be busy and needs more time for the last
            // write so we can retry writing the data, this is why we
            // use a while loop here

            // check for max retry and skip this byte
            if (retryTimeOut == MCHP24AA512_RETRY_MAX)
            {
                return(0);
                break;
            }
            
            else
                retryTimeOut++;
        }

        if (status == I2C1_MESSAGE_FAIL)
        {
            return(0);
            break;
        }

        
        address++;

    }
    return(1);

}

void readConf()
{
    uint8_t buffer[14];
    MCHP24AA512_Read(0, buffer, 14); // Leo de la eeprom las primeros 14 direcciones
    semConf.IntN_ini=buffer[2];
    semConf.IntN_fin=buffer[3];
    semConf.TV1=buffer[4];
    semConf.TV2=buffer[5];
    semConf.TV3=buffer[6];
    semConf.TV4=buffer[7];  
    semConf.T_amar=buffer[8];
    semConf.Peat_ini=buffer[9];
    semConf.IntF=buffer[10];
    semConf.modo=buffer[11];
    semConf.Peat_fin=buffer[12];
    semConf.ret_T1=buffer[13];
}
void writeConf()
{   uint8_t buffer[14];
    buffer[2]=semConf.IntN_ini;
    buffer[3]=semConf.IntN_fin;
    buffer[4]=semConf.TV1;
    buffer[5]=semConf.TV2;
    buffer[6]=semConf.TV3;
    buffer[7]=semConf.TV4;  
    buffer[8]=semConf.T_amar;
    buffer[9]=semConf.Peat_ini;
    buffer[10]=semConf.IntF;
    buffer[11]=semConf.modo;
    buffer[12]=semConf.Peat_fin;
    buffer[13]=semConf.ret_T1;
    MCHP24AA512_Write(0, buffer, 14); // Escrivo la eeprom las primeros 14 direcciones
}
uint16_t readLastAddEv()
{
    uint16_t add;
    uint8_t buffer[2];
    MCHP24AA512_Read(26, buffer,2); // Leo la direccion del ultimo evento
    add=(buffer[1]<<8)+buffer[0];
    return(add);
}
void readEvent(uint16_t add)
{
    uint8_t buffer[8];
    MCHP24AA512_Read(add, buffer,8); // Leo el evento de la direccion add
    semConf.ev_anio=buffer[0];
    semConf.ev_mes=buffer[1];
    semConf.ev_dia=buffer[2];
    semConf.ev_hor=buffer[3];
    semConf.ev_min=buffer[4];
    semConf.ev_seg=buffer[5];
    semConf.codigo=buffer[6];
}
void writeEvent(uint8_t cod)
{
    uint16_t add;
    uint8_t buffer[8];
    add=readLastAddEv();
    if (add+8>EEPROM_SIZE) // Si la memoria esta llena reinicio desde 28
        add=28;
    buffer[0]=semConf.anios;
    buffer[1]=semConf.meses;
    buffer[2]=semConf.date;
    buffer[3]=semConf.horas;
    buffer[4]=semConf.minutos;
    buffer[5]=semConf.segundos;
    buffer[6]=cod;
    buffer[7]=0xEE;
    MCHP24AA512_Write(add+8, buffer, 8); // Guardo Evento
    add=add+8;
    buffer[0]=add;
    buffer[1]=add>>8;
    MCHP24AA512_Write(26, buffer, 2); // Guardo direccion ultimo evento
}

void initEeprom()
{   uint8_t buffer[29];
    buffer[0] = 0xAA;
    buffer[1] = 0xBB;
    buffer[2]=0;
    buffer[3]=6;
    buffer[4]=15;
    buffer[5]=15;
    buffer[6]=15;
    buffer[7]=15;
    buffer[8]=2;
    buffer[9]=11;
    buffer[10]=4;
    buffer[11]=2;
    buffer[12]=11;
    buffer[13]=0;
    buffer[14]=0;
    buffer[15]=0;
    buffer[16]=0;
    buffer[17]=0;
    buffer[18]=0;
    buffer[19]=0;
    buffer[20]=0;
    buffer[21]=0;
    buffer[22]=0;
    buffer[23]=0;
    buffer[24]=0;
    buffer[25]=0;
    buffer[26]=28;
    buffer[27]=0;
    buffer[28]=0xEE;
    
    MCHP24AA512_Write(0, buffer, 29); // Escrivo la eeprom las primeros 14 direcciones
    
}