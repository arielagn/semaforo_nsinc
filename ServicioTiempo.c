/*
 * File:   ServicioTiempo.c
 * Author: LPT13
 *
 * Created on November 21, 2017, 9:29 AM
 */


#include "xc.h"
#include "ServicioTiempo.h"
#include "ControlSem.h"
#include "Bq32k.h"
#include "mcc_generated_files/uart2.h"
#include <string.h>
#include <stdio.h>
//#include "A24LC512.h"

static GPS_status GPS_getFrame_st; // variable estados ahora local
GPSData GPSDataSem; // variable con info del GPS, ahora local

static char GPS_buffer[180]; //estaba en 80. 

static uint8_t GPS_buffIndx=0;
extern Sem_Conf semConf;

void GPS_recive(uint8_t dataReciv)
{
    int i,j,k;
    char str[10][15]; 
    switch (GPS_getFrame_st)
    {
        case en_espera:
            //GPS_getFrame_st++;
            break;
        case esperando_inicio:

            if (dataReciv=='$') GPS_getFrame_st=esperando_cabecera; //detecto comienzo de trama
            break;
        case esperando_cabecera:
            GPS_buffer[GPS_buffIndx]=dataReciv;
            //GPS_buffer[GPS_buffIndx+1]=0;
            GPS_buffIndx++; 
            if (GPS_buffIndx==5)
            {
                GPS_buffIndx=0;
                i=memcmp(GPS_buffer,"GPRMC",5);
                if (i==0) GPS_getFrame_st=esperando_fin;
                else GPS_getFrame_st=esperando_inicio;
            }
            break;
        case esperando_fin:  
            GPS_buffer[GPS_buffIndx]=dataReciv;
            GPS_buffer[GPS_buffIndx+1]=0;   
            GPS_buffIndx++;
            if (dataReciv=='*') GPS_getFrame_st=interpretando_frame;
            if (GPS_buffIndx==79) 
            {
                GPS_getFrame_st=esperando_inicio;
                GPS_buffIndx=0;
            }
            break;
        case interpretando_frame:

            j=0; k=0;
            for(i=0;i<GPS_buffIndx;i++){
                //if (j>9 || k>9) break;
                if (GPS_buffer[i]==',') {
                    j++;
                    k=0;
                }
                str[j][k]=GPS_buffer[i];
                str[j][k+1]=0;
                k++;
            }
            GPSDataSem.hora=(str[1][1]-48)*10+(str[1][2]-48)-3;
            if (GPSDataSem.hora>100) GPSDataSem.hora=GPSDataSem.hora+24;
            GPSDataSem.min=(str[1][3]-48)*10+(str[1][4]-48);
            GPSDataSem.seg=(str[1][5]-48)*10+(str[1][6]-48);
            GPSDataSem.valid=str[2][1];
            for (i=0;i<10;i++)
            {
                GPSDataSem.latitud[i]=str[3][i+1];
                GPSDataSem.longitud[i]=str[5][i+1];
            }
            GPSDataSem.latitud[10]=str[4][1];
            GPSDataSem.longitud[10]=str[6][1];
            GPSDataSem.latitud[11]=str[4][2];
            GPSDataSem.longitud[11]=str[6][2];
            GPSDataSem.dia=(str[9][1]-48)*10+(str[9][2]-48);
            GPSDataSem.mes=(str[9][3]-48)*10+(str[9][4]-48);
            GPSDataSem.anio=(str[9][5]-48)*10+(str[9][6]-48);


            //if (GPSDataSem.valid!='A')
            //   writeEvent(20,r);
            GPS_getFrame_st=fin_detenido;
            GPS_buffIndx=0;

            if (GPSDataSem.valid=='A')
            {
                GPSDataSem.reint=0;
                semConf.horas=GPSDataSem.hora;
                semConf.minutos=GPSDataSem.min;
                semConf.segundos=GPSDataSem.seg;
                semConf.date=GPSDataSem.dia;
                semConf.meses=GPSDataSem.mes;
                semConf.anios=GPSDataSem.anio;
                semConf.actualizar=1;
                //BQ32k_set_time(); 
                //BQ32k_set_date();
            }   
            else
            {
                if (GPSDataSem.reint<200)   //solo para que no desborde
                    GPSDataSem.reint++;
            }

            break;
        case fin_detenido:

            break;
    }
    
    
}
//Funcion que lee la trama del GPS.
// Es multitasking pero evalua el estado solo si ha recivido algun caracter
// por UART
// Actualiza la info en GPSDataSem y del RTC si termina bien. 

void GPS_task()
{
    
    //printf("%d",GPS_getFrame_st);
    // Time out de comunicacion con GPS
    static uint32_t timeout;
    if (GPS_getFrame_st>0 && GPS_getFrame_st<5)
    {
        if (timeout>=200000)
        {
            GPSDataSem.valid='F';
            GPS_getFrame_st=0;
            timeout=0;
        }
        else 
            timeout++;
        
    }
    else 
        timeout=0;
    // Llamo a las funciones por si hay que actualizar
    //BQ32k_set_time(); 
    BQ32k_set_DateTime();
    
    
    
}
// Funcion para iniciar tarea de lectura
// DE aca para abajo son las funciones de interface
void GPS_initTask()
{
    if (GPS_getFrame_st==en_espera || GPS_getFrame_st==fin_detenido)
    {
       //GPSDataSem.reint=0;
        GPS_getFrame_st=esperando_inicio;
    }
        
    
}
// Funcion para obtener estado.
// y el resto de los get cosas
GPS_status GPS_getStatus()
{
    return (GPS_getFrame_st);
}
// 
uint8_t GPS_getHora()
{
    return (GPSDataSem.hora);
}
uint8_t GPS_getMin()
{
    return (GPSDataSem.min);
}
uint8_t GPS_getSeg()
{
    return (GPSDataSem.seg);
}
uint8_t GPS_getDia()
{
    return (GPSDataSem.dia);
}
uint8_t GPS_getMes()
{
    return (GPSDataSem.mes);
}
uint8_t GPS_getAnio()
{
    return (GPSDataSem.anio);
}
char GPS_getValid()
{
    return (GPSDataSem.valid);
}
uint8_t GPS_getReint()
{
    return (GPSDataSem.reint);
}

