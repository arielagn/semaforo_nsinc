/*
 * File:   i2cDevices.c
 * Author: LPT13
 *
 * Created on December 4, 2017, 3:55 PM
 */


#include "xc.h"
#include "i2cDevices.h"
#include "mcc_generated_files/i2c2.h"
#include "mcc_generated_files/i2c1.h"
#include "ControlSem.h"

///////////////////////////////////////////////////////////////////////////////
#define i9555_i2c_addr_wr  0x20 //+i9555_A2*8+i9555_A1*4+i9555_A0*2
#define i9555_reg_inP0  0
//#define i9555_reg_inP1  1

#define i9555_reg_outP0 2
//#define i9555_reg_outP1 3

#define i9555_reg_polP0 4
//#define i9555_reg_polP1 5

#define i9555_reg_confP0 6
//#define i9555_reg_confP1 7
//////////////////////////////////////////////////////////////////////////////
#define BQ32K_CAL_CFG1      0x07   
#define i2c_w               0x68
#define BQ32K_SF_KEY1       0x20   
#define BQ32K_SECONDS       0x00   /* Seconds register address */
//////////////////////////////////////////////////////////////////////////////
#define MCHP24AA512_ADDRESS         0x51 // slave device address

/// Funciones locales
uint8_t bin2bcd(uint8_t binary_value);
uint8_t bcd2bin(uint8_t bcd_value);

static uint8_t i2cDev_st;
uint8_t     writeBuffer[8];
/// Variables internas para comunicacion con expansor
static uint8_t port0,port1;
/// Variables internas para comunicacion con RTC
static uint8_t segundos,minutos,horas,date,meses,anios;
/// Variables internas para Configuracion y eventos EEPROM
//static uint8_t II,FI,TV1,TV2,TV3,TV4,TA,IP,MD,CM,FP,RD;
//static uint8_t HBP,LBP,anioEV,mesEV,diaEV,horaEV,minEV,segEV,codEV;
static uint16_t addr;
static uint8_t data;
/// Variables internas para sensor TEMP. 
static uint8_t temp8;
uint16_t temp16;
//// Estados para i2c
I2C2_MESSAGE_STATUS status2;
I2C1_MESSAGE_STATUS status1;

uint8_t i2cDevices_tsk()
{
    switch(i2cDev_st)
    {
        case 0:     //No hace nada
            
            break;
        case 1:     //Comfigurar expander
            status2 = I2C2_MESSAGE_PENDING;
            writeBuffer[0]=i9555_reg_confP0;
            writeBuffer[1]=0b00000011;
            writeBuffer[2]=0b01000000;
            I2C2_MasterWrite(    writeBuffer,
                                3,
                                i9555_i2c_addr_wr,
                                &status2);
            i2cDev_st++;
            break;
        case 2:
            
            if (status2==I2C2_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status2==I2C2_MESSAGE_COMPLETE)
                i2cDev_st++;
            break;
        case 3:
            status2 = I2C2_MESSAGE_PENDING;
            writeBuffer[0]=i9555_reg_polP0;
            writeBuffer[1]=0x00;
            writeBuffer[2]=0x00;
            I2C2_MasterWrite(    writeBuffer,
                                3,
                                i9555_i2c_addr_wr,
                                &status2);
            i2cDev_st++;
            break;
        case 4:
            
            if (status2==I2C2_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status2==I2C2_MESSAGE_COMPLETE)
                i2cDev_st++;
            break;
        case 5:                     //Configurar BQ32K RTC
            status1 = I2C1_MESSAGE_PENDING;
            writeBuffer[0]=BQ32K_CAL_CFG1;
            writeBuffer[1]=0x40;
            writeBuffer[2]=0x00;
            writeBuffer[3]=0x00;
            I2C1_MasterWrite(    writeBuffer,
                                4,
                                i2c_w,
                                &status1);
            i2cDev_st++;
            break;
        case 6:
            
            if (status1==I2C1_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status1==I2C1_MESSAGE_COMPLETE)
                i2cDev_st++;
            break;
        case 7:
            writeBuffer[0]=BQ32K_SF_KEY1;
            writeBuffer[1]=0x5e;
            writeBuffer[2]=0xc7;
            writeBuffer[3]=0x01;
            I2C1_MasterWrite(    writeBuffer,
                                4,
                                i2c_w,
                                &status1);
            i2cDev_st++;
            break;
        case 8:
            
            if (status1==I2C1_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status1==I2C1_MESSAGE_COMPLETE)
                i2cDev_st=101;
            break;
        case 9:         // Leer Los puertos del expansor
            status2 = I2C2_MESSAGE_PENDING;
            writeBuffer[0]=i9555_reg_inP0;
            I2C2_MasterWrite(    writeBuffer,
                                1,
                                i9555_i2c_addr_wr,
                                &status2);
            i2cDev_st++;
            break;
        case 10:
            
            if (status2==I2C2_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status2==I2C2_MESSAGE_COMPLETE)
            {
                I2C2_MasterRead(     writeBuffer,
                            2,
                            i9555_i2c_addr_wr,
                            &status2);
                i2cDev_st++;
                status2 = I2C2_MESSAGE_PENDING;
            }
            break;
        case 11:
            
            if (status2==I2C2_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status2==I2C2_MESSAGE_COMPLETE)
            {
                port0=writeBuffer[0];
                port1=writeBuffer[1];
                i2cDev_st=101;
            }
            break;
        case 12:        //Escribir en los puertos
            status2 = I2C2_MESSAGE_PENDING;
            writeBuffer[0]=i9555_reg_outP0;
            writeBuffer[1]=port0;
            writeBuffer[2]=port1;
            i2cDev_st++;
            I2C2_MasterWrite(    writeBuffer,
                                3,
                                i9555_i2c_addr_wr,
                                &status2);
            break;
        case 13:
            
            if (status2==I2C2_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status2==I2C2_MESSAGE_COMPLETE)
                i2cDev_st=101;
            break;
        case 14: // Set Fecha y Hora en BQ32K
            status1 = I2C1_MESSAGE_PENDING;
            uint8_t hrs,min,seg,day,mth,year;
            seg=  segundos & 0x7F;
            min=  minutos & 0x7F;
            hrs=  horas & 0x3F;
            day=  date & 0x3F;
            mth=  meses & 0x1F;
            year= anios & 0xFF;
            writeBuffer[0]=BQ32K_SECONDS;
            writeBuffer[1]=bin2bcd(seg);
            writeBuffer[2]=bin2bcd(min);
            writeBuffer[3]=bin2bcd(hrs);
            writeBuffer[4]=0;
            writeBuffer[5]=bin2bcd(day);
            writeBuffer[6]=bin2bcd(mth);
            writeBuffer[7]=bin2bcd(year);
            I2C1_MasterWrite(    writeBuffer,
                                8,
                                i2c_w,
                                &status1);
            i2cDev_st++;
            break;
        case 15:
            
            if (status1==I2C1_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status1==I2C1_MESSAGE_COMPLETE)
                i2cDev_st=101;
            break;
        case 16:        //Leer Fecha y hora del BQ32K
            status1 = I2C1_MESSAGE_PENDING;
            writeBuffer[0]=BQ32K_SECONDS;
            I2C1_MasterWrite(    writeBuffer,
                                1,
                                i2c_w,
                                &status1);
            i2cDev_st++;
            break;
        case 17:
            
            if (status1==I2C1_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status1==I2C1_MESSAGE_COMPLETE)
                i2cDev_st++;
            break;
        case 18:
            status1 = I2C1_MESSAGE_PENDING;
            I2C1_MasterRead(     writeBuffer,
                            8,
                            i2c_w,
                            &status1);
            i2cDev_st++;
            break;
        case 19:
            if (status1==I2C1_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status1==I2C1_MESSAGE_COMPLETE)
            {
                segundos = bcd2bin(writeBuffer[0] & 0x7f);   
                minutos  = bcd2bin(writeBuffer[1] & 0x7f);  
                horas  = bcd2bin(writeBuffer[2] & 0x3f);   
                date = bcd2bin(writeBuffer[4] & 0x7f);   
                date  = bcd2bin(writeBuffer[5] & 0x3f);   
                meses  = bcd2bin(writeBuffer[6] & 0x1f);  
                anios = bcd2bin(writeBuffer[7]); 
                i2cDev_st=101;
            }
            
            break;
        case 20: // Escribir en EEPROM
            status1 = I2C1_MESSAGE_PENDING;
            writeBuffer[0] = (addr >> 8);                // high address
            writeBuffer[1] = (uint8_t)(addr);            // low low address
            writeBuffer[2] = data;
            I2C1_MasterWrite(    writeBuffer,
                                    3,
                                    MCHP24AA512_ADDRESS,
                                    &status1);
            i2cDev_st++;
            break;
        case 21:
            
            if (status1==I2C1_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status1==I2C1_MESSAGE_COMPLETE)
                i2cDev_st=101;
            break;
        case 22:        // Leer de la EEPROM
            status1 = I2C1_MESSAGE_PENDING;
            writeBuffer[0] = (addr >> 8);                // high address
            writeBuffer[1] = (uint8_t)(addr);            // low low address
            I2C1_MasterWrite(    writeBuffer,
                                    2,
                                    MCHP24AA512_ADDRESS,
                                    &status1);
            i2cDev_st++;
            break;
        case 23:
            
            if (status1==I2C1_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status1==I2C1_MESSAGE_COMPLETE)
                i2cDev_st++;
            break;
        case 24:
            status1 = I2C1_MESSAGE_PENDING;
            I2C1_MasterRead(     writeBuffer,
                                        1,
                                        MCHP24AA512_ADDRESS,
                                        &status1);
            i2cDev_st++;
            break;
        case 25:
            if (status1==I2C1_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status1==I2C1_MESSAGE_COMPLETE)
            {   data=writeBuffer[0];
                i2cDev_st=101;
            }
            break;
        case 26:        // Lectura de Temperatura
            status1 = I2C1_MESSAGE_PENDING;
            I2C1_MasterRead(     writeBuffer,
                        2,
                        0x48,
                        &status1);
            i2cDev_st++;
            break;
        case 27:
            
            if (status1==I2C1_MESSAGE_FAIL)
                i2cDev_st=100;
            if (status1==I2C1_MESSAGE_COMPLETE)
                i2cDev_st++;
            break;
        case 28:
            temp16 = writeBuffer[0] << 8;
            temp16 = temp16 | writeBuffer[1];
            temp8 = (temp16 >> 8);
            i2cDev_st=101;
            break;
        case 100:   //Falla de configuracion
            break;
        case 101:   //Termina la transaccion
            break;
        default:
            break;
    }
return (i2cDev_st);    
}

// Para conversion del BQ32K

uint8_t bin2bcd(uint8_t binary_value) 
{ 
  uint8_t temp; 
  uint8_t retval; 

  temp = binary_value; 
  retval = 0; 

  while(1) 
  { 
    // Get the tens digit by doing multiple subtraction 
    // of 10 from the binary value. 
    if(temp >= 10) 
    { 
      temp -= 10; 
      retval += 0x10; 
    } 
    else // Get the ones digit by adding the remainder. 
    { 
      retval += temp; 
      break; 
    } 
  } 

  return(retval); 
} 

// Input range - 00 to 99. 
uint8_t bcd2bin(uint8_t bcd_value)
{ 
 uint8_t temp; 

  temp = bcd_value; 
  // Shifting upper digit right by 1 is same as multiplying by 8. 
  temp >>= 1; 
  // Isolate the bits for the upper digit. 
  temp &= 0x78; 

  // Now return: (Tens * 8) + (Tens * 2) + Ones 

  return(temp + (temp >> 2) + (bcd_value & 0x0f));
  
    /*return (((bcd_value >> 4)*10)+(bcd_value & 0x0f)); */

}
/*  Estas son las funciones de interface a usar
    
    Estas no llaman a las funciones del RTC solo trabajan con la estructura en
 memoria
 */

uint8_t getSegundos()
{
    //BQ32k_get_time();
    return (segundos);
}

uint8_t getMinutos()
{
    //BQ32k_get_time();
    return (minutos);
}
uint8_t getHoras()
{
    //BQ32k_get_time();
    return (horas);
}
uint8_t getDate()
{
    //BQ32k_get_date();
    return (date);
}
uint8_t getMeses()
{
    //BQ32k_get_date();
    return (meses);
}
uint8_t getAnios()
{
    //BQ32k_get_date();
    return (anios);
}
void setSegundos(uint8_t d)
{
    segundos=d;
    //BQ32k_set_time();
}
void setMinutos(uint8_t d)
{
    minutos=d;
    //BQ32k_set_time();
}
void setHoras(uint8_t d)
{
    horas=d;
    //BQ32k_set_time();
}
void setDate(uint8_t d)
{
    date=d;
    //BQ32k_set_date();
}
void setMeses(uint8_t d)
{
    meses=d;
    //BQ32k_set_date();
}
void setAnios(uint8_t d)
{
    anios=d;
    //BQ32k_set_date();
}
/// Interface para expansor
void setExpander (uint8_t p0,uint8_t p1)
{
    port0=p0;
    port1=p1;
}
uint8_t getP0Expander()
{
    return (port0);
}
uint8_t getP1Expander()
{
    return (port1);
}
/// Interface para EEPROM
void setAddDataEprom(uint16_t add,uint8_t dat)
{
    addr=add;
    data=dat;
}
uint8_t getEeprom()
{
    return(data);
}
/// Interface para TEMP
uint8_t getTemp()
{
    return (temp8);
}

///// Interfaces de tarea
void seti2cDevices(uint8_t st)
{
    i2cDev_st=st;
}
uint8_t geti2cDevices()
{
    return (i2cDev_st);
}
///// Funciones mas especificas para la eeprom
void iniFctEeprom() // Funcion que grava valores por defecto en eeprom
{
    uint8_t dts[]={0xAA,0xBB,0,6,15,15,15,15,2,7,4,2,13,0,0,0,0,0,0,0,0,0,0,0,0,
    0,28,0,0xEE};
    addr=0;
    for (addr=0;addr<=28;addr++)
    {
        data=dts[addr];
        if (geti2cDevices()>=100) // Esta desocupado el i2C?
            seti2cDevices(20); 
        while (101!=geti2cDevices())
        {
            i2cDevices_tsk();

            ClrWdt();
        }
    }
    
}
void readCnfEeprom(Sem_Conf *p)
{
    uint8_t dts[14];
    for (addr=0;addr<=13;addr++)
    {
        if (geti2cDevices()>=100) // Esta desocupado el i2C?
            seti2cDevices(22); 
        while (101!=geti2cDevices())
        {
            i2cDevices_tsk();

            ClrWdt();
        }
        dts[addr]=data;
    }
    
    p->IntN_ini=dts[2];
    p->IntN_fin=dts[3];
    p->TV1=dts[4];
    p->TV2=dts[5];
    p->TV3=dts[6];
    p->TV4=dts[7];
    p->T_amar=dts[8];
    p->Peat_ini=dts[9];        
    p->IntF=dts[10];
    p->modo=dts[11];
    p->Peat_fin=dts[12];
    p->ret_T1=dts[13];
    
   
}