/*
 * File:   usbComm.c
 * Author: LPT13
 *
 * Created on January 11, 2018, 8:06 PM
 */


#include "xc.h"
#include "mcc_generated_files/uart1.h"
#include "ControlSem.h"
#include "A24LC512.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Para tarea comunicacion USB
uint8_t commUSB_task_st;
const uint8_t iniBuffer[31] = "--Lista de Eventos Semaforo--\n\r" ;
const uint8_t finBuffer[29] = "--Fin de lista de Eventos--\n\r";
char linea_ev[38]="00/00/00 00:00:00 - Baja tension VAC\n\r";
uint8_t numBytes;
extern Sem_Conf semConf;
extern char const  *Eventos [29][17] ;
static uint16_t punt_E=28;
void commUSB_task(void)
{
    char dataReciv;
    
    switch (commUSB_task_st)
    {
        case 0:
            if( UART1_RX_DATA_AVAILABLE & UART1_StatusGet() )
            {   
                dataReciv=UART1_Read();
                if (dataReciv=='$') commUSB_task_st++;
                if (dataReciv=='&') commUSB_task_st=5;
            }
            numBytes=0;
            break;
        case 1:
            if( !(UART1_TX_FULL & UART1_StatusGet()) )
            {
                UART1_Write(iniBuffer[numBytes++]);
            }
            if (numBytes==31) 
            {
                punt_E=readLastAddEv();
                
                commUSB_task_st++; numBytes=0; 
            }
            break;
        case 2:
            readEvent(punt_E);
            sprintf(linea_ev,"%02d/%02d/%02d %02d:%02d:%02d -%s\n\r",semConf.ev_dia,semConf.ev_mes,semConf.ev_anio,semConf.ev_hor,semConf.ev_min,semConf.ev_seg,Eventos[semConf.codigo][0]);
            commUSB_task_st++;
            numBytes=0;
            break;
        case 3:
            if( !(UART1_TX_FULL & UART1_StatusGet()) )
            {
                UART1_Write(linea_ev[numBytes++]);
            }
            if (numBytes==38) 
            {
                punt_E=punt_E-8;
                if (punt_E<=28) 
                {
                    numBytes=0; commUSB_task_st++;
                }
                else 
                {
                    numBytes=0; commUSB_task_st=2;
                }
            }
            
            break;
        case 4:
            if( !(UART1_TX_FULL & UART1_StatusGet()) )
            {
                UART1_Write(finBuffer[numBytes++]);
            }
            if (numBytes==29) 
            {
                numBytes=0;
                commUSB_task_st=0;
            }
            break;
        case 5:
            //sprintf(linea_ev,"-%x-%x-%x-%x-%x!!-\n\r",
            //     getBufferAD(0),getBufferAD(1),getBufferAD(2),getBufferAD(3),getBufferAD(4));
            numBytes=0;
            commUSB_task_st++;
            break;
        case 6:
            if( !(UART1_TX_FULL & UART1_StatusGet()) )
            {
                UART1_Write(linea_ev[numBytes++]);
            }
            if (numBytes==23) 
            {
                numBytes=0;
                commUSB_task_st=0;
            }
            break;
        default:
            break;
    }
}
void set_commUSB_st(uint8_t st)
{
    commUSB_task_st=st;
}
uint8_t get_commUSB_st(void)
{
    return(commUSB_task_st);
}
