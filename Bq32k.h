/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef BQ32K_H
#define	BQ32K_H

#include <xc.h> // include processor files - each processor file is guarded.  

// TODO Insert appropriate #include <>

// TODO Insert C++ class definitions if appropriate

// TODO Insert declarations
#define BQ32K_SECONDS         0x00   /* Seconds register address */
#define BQ32K_MINUTES         0x01   /* Minutes register address */
#define BQ32K_CENT_HOURS      0x02   /* Century flag */
#define BQ32K_DAY             0x03   /* Century flag */
#define BQ32K_DATE            0x04   /* Century flag */
#define BQ32K_MONTH           0x05   /* Century flag */
#define BQ32K_YEARS           0x06   /* Century flag */
#define BQ32K_CAL_CFG1        0x07   /* Century flag */
#define BQ32K_TCH2            0x08   /* Century flag */
#define BQ32K_CFG2            0x09   /* Century flag */
#define BQ32K_SF_KEY1         0x20   /* Century flag */
#define BQ32K_SF_KEY2         0x21   /* Century flag */
#define BQ32K_SFR             0x22   /* Century flag */

#define BQ32K_SECONDS_MASK    0x7F   /* Mask over seconds value */
#define BQ32K_STOP            0x80   /* Oscillator Stop flat */

#define BQ32K_MINUTES_MASK    0x7F   /* Mask over minutes value */
#define BQ32K_OF              0x80   /* Oscillator Failure flag */

#define BQ32K_HOURS_MASK      0x3F   /* Mask over hours value */
#define BQ32K_CENT_EN         0x80   /* Century flag enable bit */

#define BQ32K_DEVICE_TIMEOUT    500
#define BQ32K_RETRY_MAX         100 

//#define i2c_r            0xD1   /*  */
#define i2c_w           0x68 //0xD0   /*  */



// Comment a function and leverage automatic documentation with slash star star
/**
    <p><b>Function prototype:</b></p>
  
    <p><b>Summary:</b></p>

    <p><b>Description:</b></p>

    <p><b>Precondition:</b></p>

    <p><b>Parameters:</b></p>

    <p><b>Returns:</b></p>

    <p><b>Example:</b></p>
    <code>
 
    </code>

    <p><b>Remarks:</b></p>
 */
// TODO Insert declarations or function prototypes (right here) to leverage 
// live documentation
uint8_t BQ32k_init();
uint8_t BQ32k_set_DateTime();
//uint8_t BQ32k_set_date();
uint8_t BQ32k_get_time();
uint8_t BQ32k_get_date();
uint8_t bin2bcd(uint8_t binary_value); 
uint8_t bcd2bin(uint8_t bcd_value);


#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

