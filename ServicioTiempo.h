/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef SERVICIOTIEMPO_H
#define	SERVICIOTIEMPO_H

#include <xc.h> // include processor files - each processor file is guarded.  


// TODO Insert appropriate #include <>

// TODO Insert C++ class definitions if appropriate

// TODO Insert declarations
typedef struct {
    uint8_t hora;
    uint8_t min;
    uint8_t seg;
    char    latitud[11];    //Estos dos estan en texto solo para mostrarlo 
    char    longitud[12];   //no tiene uso para el funcionamiento del semaforo
    char valid;             //A: valid V: not valid F: Fail comm to GPS
    uint8_t dia;        // Solo actualizable desde RTC
    uint8_t mes;
    uint8_t anio;
    uint8_t reint;      // Reintentos de captacion correcta con Satelites
} GPSData;
// Estos son los los estados de GPS_task
typedef enum gps_status{
    en_espera            = 0,
    esperando_inicio     = 1,
    esperando_cabecera   = 2,
    esperando_fin        = 3,
    interpretando_frame  = 4,
    fin_detenido         = 5
} GPS_status;
// Comment a function and leverage automatic documentation with slash star star
/**
    <p><b>Function prototype:</b></p>
  
    <p><b>Summary:</b></p>

    <p><b>Description:</b></p>

    <p><b>Precondition:</b></p>

    <p><b>Parameters:</b></p>

    <p><b>Returns:</b></p>

    <p><b>Example:</b></p>
    <code>
 
    </code>

    <p><b>Remarks:</b></p>
 */
// TODO Insert declarations or function prototypes (right here) to leverage 
// live documentation
// Funcion de configuracion de GPS. No usar no probado a fondo. 
// La programe solo por si aparece algun modulo GPS con algun seteo incorrecto. 
// La idea es ponerlo a 4800 baudios y moso MNEA
//void GPS_Init();

// Funcion multitasking con los siguientes estados. No necesita arrancarla
// 0: no hace nada.
// 1: arranca a buscar $ en la trama
// 2: busca las tramas con GPGGA
// 3: copia a buffer hasta *
// 4: extre los datos y actualiza estructura, 
// 5: para final
void GPS_task();
void GPS_recive(uint8_t);
void GPS_initTask();
GPS_status GPS_getStatus();
uint8_t GPS_getHora();
uint8_t GPS_getMin();
uint8_t GPS_getSeg();
uint8_t GPS_getDia();
uint8_t GPS_getMes();
uint8_t GPS_getAnio();
char GPS_getValid();
uint8_t GPS_getReint();

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

