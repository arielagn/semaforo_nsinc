/*
 * File:   LCDGeneric.c
 * Author: LPT13
 *
 * Created on November 11, 2017, 5:02 PM
 */


#include "xc.h"
#include "mcc_generated_files/tmr5.h"
#include "mcc_generated_files/tmr4.h"
#include "mcc_generated_files/pin_manager.h"
#include "LCDGeneric.h"

uint8_t NLinea;

void vWriteLCD(uint8_t Data,uint8_t Type)
{ 
    //delay_us(100);
    if(Type){
	LCDRS_SetHigh(); 
    }else{
	LCDRS_SetLow();
    }
		
    if (Data & 0x80) LCD07_SetHigh(); else LCD07_SetLow();
    if (Data & 0x40) LCD06_SetHigh(); else LCD06_SetLow();
    if (Data & 0x20) LCD05_SetHigh(); else LCD05_SetLow();
    if (Data & 0x10) LCD04_SetHigh(); else LCD04_SetLow();
                	
    delay_us(1);
    LCDRE_SetHigh();
    delay_us(1);
    LCDRE_SetLow();

    if (Data & 0x08) LCD07_SetHigh(); else LCD07_SetLow();
    if (Data & 0x04) LCD06_SetHigh(); else LCD06_SetLow();
    if (Data & 0x02) LCD05_SetHigh(); else LCD05_SetLow();
    if (Data & 0x01) LCD04_SetHigh(); else LCD04_SetLow();
	
    delay_us(1);
    LCDRE_SetHigh();
    delay_us(1);
    LCDRE_SetLow();
}

void vInitLCD(){
    uint8_t i;
    delay_ms(16);
    
    NLinea=1;
    LCD07_SetLow();
    LCD06_SetLow();
    LCD05_SetLow();
    LCD04_SetLow();
	
    LCDRS_SetLow();
    LCDRE_SetLow();
   

    LCD05_SetHigh();
    LCD04_SetHigh();

	for(i=0;i<3;i++){
		LCDRE_SetHigh(); 
		delay_ms(2);
        LCDRE_SetLow(); 
	 	delay_ms(2);
	}

	LCD04_SetLow();

	LCDRE_SetHigh(); 
	delay_us(10);
	LCDRE_SetLow(); 
	delay_us(10);
	vWriteLCD(0x20 | (LcdType<<2),LCD_COMMAND);  // Tipo display.-  
    delay_ms(4); 
	vWriteLCD(0x01,LCD_COMMAND);	// Borramos display.-   
	delay_ms(4);           
	vWriteLCD(0x06,LCD_COMMAND);	// Incrementa cursor.-
	delay_ms(4);
	vWriteLCD(0x0C,LCD_COMMAND);	// Encendemos display.-
	delay_ms(100); //4
}


void vPutc_LCD(uint8_t Data){
	//Se pueden definir los comandos necesarios agregando mas "cases"
	switch(Data){
		case '\f':   //Comando borra lcd
			vWriteLCD(0x01,LCD_COMMAND);
			NLinea=1;
			delay_ms(2);
		break;
		case '\n':	//comando linea nueva
			vGotoxyLCD(1,++NLinea);			
		break;
		case '\b':	//Comando Cursor on
			vWriteLCD(0x0D,LCD_COMMAND);		//vWriteLCD(0x0F,LCD_COMMAND);	// Encendemos display c/cursor blinking	
		break;
		case '\r':	//Comando Cursor off
			vWriteLCD(0x0C,LCD_COMMAND);
                        delay_ms(2);
		break;
		default:
			vWriteLCD(Data,LCD_DATA);
	}
} 

void vGotoxyLCD(uint8_t x,uint8_t y){
uint8_t Direccion;

	switch(y){
		case 1:Direccion = LCD_LINE_1_ADDRESS;NLinea=1;break;
		case 2:Direccion = LCD_LINE_2_ADDRESS;NLinea=2;break;
		
		default:Direccion = LCD_LINE_1_ADDRESS;NLinea=1;break;
	}

	Direccion+=x-1;
	//__delay_us(100); Ojo con esto Ariel
	vWriteLCD(0x80|Direccion,LCD_COMMAND);
}

void vPuts_LCD(char *buffer){

    while(*buffer != '\0'){
	     //vWriteLCD(*buffer++,LCD_DATA);
	     vPutc_LCD(*buffer++);
    }
}

void delay_us(uint16_t del)
{
    TMR5_Period16BitSet( 4*del );  // 0x0004 son 1us
    while (!TMR5_GetElapsedThenClear())
        TMR5_Tasks_16BitOperation();
}
void delay_ms(uint16_t del)
{
    TMR4_Period16BitSet( 0x01F4*del);  // 0x01F4 son 1ms
        while (!TMR4_GetElapsedThenClear())
            TMR4_Tasks_16BitOperation();
}