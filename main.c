/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC24 / dsPIC33 / PIC32MM MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - pic24-dspic-pic32mm : v1.35
        Device            :  PIC24FJ64GA104
    The generated drivers are tested against the following:
        Compiler          :  XC16 1.31
        MPLAB             :  MPLAB X 3.60
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#include "mcc_generated_files/mcc.h"
#include "menu.h"
#include "ServicioTiempo.h"
#include "ControlSem.h"
#include "LCDGeneric.h"
#include "Bq32k.h"
#include "PCA9555.h"
#include "A24LC512.h"
#include "usbComm.h"
#include "supervisor.h"
#include "analog.h"
#include <stdio.h>
/*
                         Main application
 */

//static Sem_Conf semConf;

uint8_t buffer[8];
int main(void)
{
    // initialize the device
    SYSTEM_Initialize();
    
    vInitLCD();
    // Inicializacion de EXPANDER y BQ32K
    BQ32k_init();
    i9555_Init();
    
    readConf();
    
    BQ32k_get_date();
    BQ32k_get_time();
    
    writeEvent(17);
    GPS_initTask();
    setAnalogTaskSt(0);
    
    while (1)
    {
        tecla_tsk();
        menu_tsk();
        GPS_task();
        commUSB_task();
        sem_task();
        supervisor();
        analogTask();
        ClrWdt();
        //LED2_Toggle();
    }

    return -1;
}
/**
 End of File
*/