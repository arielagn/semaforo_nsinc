/*
 * File:   TMP175.c
 * Author: LPT13
 *
 * Created on November 20, 2017, 2:20 PM
 */


#include "TMP175.h"
#include "mcc_generated_files/i2c1.h"


int8_t temp()
{
    uint16_t    retryTimeOut, slaveTimeOut;
    I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
    uint8_t readBuffer[2];
    int16_t temp;  
    retryTimeOut = 0;
    slaveTimeOut = 0;
    while(status != I2C1_MESSAGE_FAIL)
    {
        I2C1_MasterRead(     readBuffer,
                        2,
                        0x48,
                        &status);

        while(status == I2C1_MESSAGE_PENDING)
        {
            if (slaveTimeOut == TMP175_DEVICE_TIMEOUT)
                return (0);
            else
                slaveTimeOut++;
        }

        if (status == I2C1_MESSAGE_COMPLETE)
            break;

        if (retryTimeOut == TMP175_RETRY_MAX)
            return(0);
        else
            retryTimeOut++;
    }
    // exit if the last transaction failed
    if (status == I2C1_MESSAGE_FAIL)
        return(0);

    temp = readBuffer[0] << 8;
    temp = temp | readBuffer[1];
    return(temp >> 8);
    
    
} 

