/*
 * File:   menu.c
 * Author: LPT13
 *
 * Created on November 25, 2017, 5:33 PM
 */


#include "xc.h"
#include "LCDGeneric.h"
#include "PCA9555.h"
#include "menu.h"
#include "Bq32k.h"
#include "A24LC512.h"
#include "ServicioTiempo.h"
#include "ControlSem.h"
#include "analog.h"
#include <stdio.h>
#include <stdbool.h>

extern Sem_Conf semConf;
static uint8_t tecla, port0, port1;
static uint8_t menu_sts,tecla_sts;
static bool wrReg,actTimeDate;
char lcd_linea1[17]={0};
char lcd_linea2[17]={0};
char const  *Eventos [29][17] = {{  "Lamp. quem. via1\0"},  //0
                                {   "Lamp. quem. via2\0"},  //1
                                {   "Lamp. quem. via3\0"},  //2
                                {   "Lamp. quem. via4\0"},  //3
                                {   "Lamp. quem. via5\0"},  //4
                                {   "Lamp. quem. via6\0"},  //5
                                {   "Baja tension VAC\0"},  //6
                                {   "CC via1/V.Cruzad\0"},  //7
                                {   "CC via2/V.Cruzad\0"},  //8
                                {   "CC via3/V.Cruzad\0"},  //9
                                {   "CC via4/V.Cruzad\0"},  //10
                                {   "CC via5/V.Cruzad\0"},  //11
                                {   "CC via6/V.Cruzad\0"},  //12
                                {   "Intermit forzada\0"},  //13
                                {   "Config./diagnost\0"},  //14
                                {   "Falla acc. memo.\0"},  //15
                                {   "Falla de superv.\0"},  //16
                                {   "Reset  sistema  \0"},  //17
                                {   "Falla exp. ptos.\0"},  //18
                                {   "Falla Com. serie\0"},  //19
                                {   "Falla Modulo GPS\0"},  //20
                                {   "Falla reloj int.\0"},  //21
                                {   "Modo RUN activo \0"},  //22
                                {   "Conf.Def/BorraEv\0"},  //23
                                {   "\0"},  //24
                                {   "\0"},  //25
                                {   "REQUIERE SERVICE\\0"}}; //26
void intTecla()
{
    tecla_sts=1;
    
   
}
uint8_t tecla_tsk()
{
    static uint8_t tec_int;
    switch(tecla_sts)
    {
        case 0:
            tec_int=0;
            break;
        case 1:
            port0=i9555_read(0);    //leo los 2 bytes del expander
            port1=i9555_read(1); 
            if (!(port0 & (1<<0)))
                tec_int=2; //Se presiono tecla T2
            if (!(port0 & (1<<1)))
                tec_int=1; //Se presiono tecla T1
            if (!(port1 & (1<<6)))
                tec_int=3; //Se presiono tecla T3
            if (tec_int!=0)
                tecla_sts++;
            break;
        case 2:
            port0=i9555_read(0);    //leo los 2 bytes del expander
            port1=i9555_read(1);
            if((port1 & (1<<6))&& (port0 & (1<<0)) && (port0 &(1<<1)))  // Se solto la tecla?
            {
                tecla=tec_int;
                tecla_sts=0;
            }
            break;
        default:
            break;             
    }
    return(tecla_sts);
}

void intMenuSeg()
{   
    actTimeDate=true;
    
}

uint8_t menu_tsk()
{
    static uint8_t date,meses,anios,horas,minutos,aux,aux1;
    static uint16_t add;
    //if (GPS_getStatus()>0 && GPS_getStatus()<5)
    //    return (menu_sts);      // Salgo si el gps esta actualizando. GPS necesita tiempo de ejecucion
    if (tecla==0 && !actTimeDate  )
        return (menu_sts);
    
    BQ32k_get_date();
    BQ32k_get_time();
    actTimeDate=false;
   
    int h,m,sg;
    switch(menu_sts)
    {
        case 0:
            
            h=semConf.horas;
            m=semConf.minutos;
            sg=semConf.segundos;
            sprintf(lcd_linea1,"%02u:%02u:%02u  %02u/%02u ",h,m,sg,semConf.date,semConf.meses);
            switch (semConf.IntF)
            {
                case 0: sprintf(lcd_linea2,"Buscando GPS %d  ",GPS_getReint()); break;
                case 1: sprintf(lcd_linea2,"Sinc. %d        ",semConf.restoDec); break;
                case 2: sprintf(lcd_linea2,"Modo Oper.  [%c] ",GPS_getValid()); break; //sprintf(lcd_linea2,"Modo Oper.%d    ",semConf.restoInc);
                case 3: sprintf(lcd_linea2,"Modo Oper.Peat. "); break;
                case 4: sprintf(lcd_linea2,"Modo Int. Forz. "); break;
                case 5: sprintf(lcd_linea2,"Modo Int. Noct. "); break;
                case 6: sprintf(lcd_linea2,"Baja Tension VAC"); break;
            }
            if (tecla==1)
            {
                if (semConf.IntF==4)
                    menu_sts=3;
                else
                    menu_sts=1;
            }
            if (tecla==2)
                menu_sts=4;
            break;
        case 1:
            sprintf(lcd_linea1,"Modo Intermit?  ");
            sprintf(lcd_linea2,"  Pres.Enter[T3]");  
            if (tecla==1)
                menu_sts=0;
            if (tecla==3)
            {
                semConf.IntF=4;  // Modo intermitente forzado
                writeConf();
                menu_sts=0; 
            }
               
            break;
        case 2:
            // Sin uso. 
            break;
        case 3:
            sprintf(lcd_linea1,"Modo Operativo? ");
            sprintf(lcd_linea2,"  Pres.Enter[T3]");
            if (tecla==1)
                menu_sts=0;
            if (tecla==3)
            {
                semConf.IntF=0;        // Modo activo
                writeEvent(22); // Gravo evento, modo run activo
                writeConf();
                menu_sts=0;
            }
                
            break;  
        case 4:
            sprintf(lcd_linea1,"Programacion?   ");
            sprintf(lcd_linea2,"  Pres.Enter[T3]");
           if (tecla==2)
                menu_sts=0;
            if (tecla==3)
            {
                writeEvent(14); // Configuracion diagnostico
                semConf.IntF=4;
                menu_sts=5;
            }
                
            break; 
        case 5:
            wrReg=false;
            sprintf(lcd_linea1,"Cantidad Movim.?");
            sprintf(lcd_linea2,"  Pres.Enter[T3]");
            if (tecla==3)
                menu_sts=13;
            if (tecla==2)
                menu_sts=6;
            break;
        case 6:
            sprintf(lcd_linea1,"Tiempos Semaforo");
            sprintf(lcd_linea2,"  Pres.Enter[T3]");
            if (tecla==3)
                menu_sts=14;
            if (tecla==2)
                menu_sts=7;
            break;
        case 7:
            sprintf(lcd_linea1,"Set Fecha y Hora");
            sprintf(lcd_linea2,"  Pres.Enter[T3]");
            if (tecla==3)
                menu_sts=20;
            if (tecla==2)
                menu_sts=8;
            break;
        case 8:
            sprintf(lcd_linea1,"Interm. Nocturna");
            sprintf(lcd_linea2,"  Pres.Enter[T3]");
            if (tecla==3)
                menu_sts=21;
            if (tecla==2)
                menu_sts=10;    // Deveria ir al 9 para funcionalidad peatonal por tiempos
            break;
        case 9:
            sprintf(lcd_linea1,"Tiempos Peatonal");
            sprintf(lcd_linea2,"  Pres.Enter[T3]");
            if (tecla==3)
                menu_sts=22;
            if (tecla==2)
                menu_sts=10;
            break;
        case 10:
            sprintf(lcd_linea1,"Ver Eventos     ");
            sprintf(lcd_linea2,"  Pres.Enter[T3]");
            if (tecla==3)
                menu_sts=23;  
            if (tecla==2)
                menu_sts=11;
            break;
        case 11:
            sprintf(lcd_linea1,"Ver GPS         ");
            sprintf(lcd_linea2,"  Pres.Enter[T3]");
            if (tecla==3)
                menu_sts=24;
            if (tecla==2)
                menu_sts=25;
            break;
        case 12:
            sprintf(lcd_linea1,"Salir Programac.");
            sprintf(lcd_linea2,"  Pres.Enter[T3]");
            if (tecla==3)
                menu_sts=0;
            if (tecla==2)
                menu_sts=5;
            break;
        case 13:
            sprintf(lcd_linea1,"Cantidad Movim. ");
            if (semConf.modo==DOS_T)
                sprintf(lcd_linea2," Dos tiempos    ");
            if (semConf.modo==TRES_T)
                sprintf(lcd_linea2," Tres tiempos   ");
            if (semConf.modo==CUATRO_T)
                sprintf(lcd_linea2," Cuatro tiempos ");
            if (semConf.modo==DOS_T_P)
                sprintf(lcd_linea2," Dos T. y Peat. ");
            if (semConf.modo==TRES_T_P)
                sprintf(lcd_linea2," Tres T. y Peat.");
            if (semConf.modo==UN_T_P)
                sprintf(lcd_linea2,"Peat. Demanda 1V");
            if (tecla==3)
            {    if (wrReg)
                    menu_sts=30;
                else 
                    menu_sts=5;
            }
            if (tecla==1)
            {
                wrReg=true;
                if(semConf.modo==TRES_T_P) // Si peatonal por demanda deberia ser UN_T_P
                    semConf.modo=DOS_T;
                else
                    semConf.modo++;
            }
                
            break;
        case 14:
            sprintf(lcd_linea1,"T. Amarillo(seg)");
            sprintf(lcd_linea2,"            [%02u]",semConf.T_amar);
            if (tecla==3)
            {
                if (wrReg)
                    menu_sts=30;
                else 
                    menu_sts=6;
            }
            if (tecla==2)
                menu_sts=15;
            if (tecla==1)
            {
                wrReg=true;
                if (semConf.T_amar==6)
                    semConf.T_amar=2;
                else
                    semConf.T_amar++;
            }
                
            break;
        case 15:
            sprintf(lcd_linea1,"T. Verde 1 (seg)");
            sprintf(lcd_linea2,"            [%02u]",semConf.TV1);
            if (tecla==3)
            {
                if (wrReg)
                    menu_sts=30;
                else 
                    menu_sts=6;
            }
            if (tecla==2)
                menu_sts=16;
            if (tecla==1)
            {
                wrReg=true;
                if (semConf.TV1==60)
                    semConf.TV1=10;
                else
                    semConf.TV1++;
            }
            break;  
        case 16:
            sprintf(lcd_linea1,"T. Verde 2 (seg)");
            sprintf(lcd_linea2,"            [%02u]",semConf.TV2);
            if (tecla==3)
            {
                if (wrReg)
                    menu_sts=30;
                else 
                    menu_sts=6;
            }
            if (tecla==2)
                menu_sts=17;
            if (tecla==1)
            {
                wrReg=true;
                if (semConf.TV2==60)
                    semConf.TV2=10;
                else
                    semConf.TV2++;
            }
            break;  
        case 17:
            sprintf(lcd_linea1,"T. Verde 3 (seg)");
            sprintf(lcd_linea2,"            [%02u]",semConf.TV3);
            if (tecla==3)
            {
                if (wrReg)
                    menu_sts=30;
                else 
                    menu_sts=6;
            }
            if (tecla==2)
                menu_sts=18;
            if (tecla==1)
            {
                wrReg=true;
                if (semConf.TV3==60)
                    semConf.TV3=10;
                else
                    semConf.TV3++;
            }
            break; 
        case 18:
            sprintf(lcd_linea1,"T. Verde 4 (seg)");
            sprintf(lcd_linea2,"            [%02u]",semConf.TV4);
            if (tecla==3)
            {
                if (wrReg)
                    menu_sts=30;
                else 
                    menu_sts=6;
            }
            if (tecla==2)
                menu_sts=19;
            if (tecla==1)
            {
                wrReg=true;
                if (semConf.TV4==60)
                    semConf.TV4=10;
                else
                    semConf.TV4++;
            }
            break;  
        case 19:
            sprintf(lcd_linea1,"Retardo s.  [%02u]",semConf.ret_T1);
            sprintf(lcd_linea2,"Per.%d    (seg)   ",semConf.ciclo);
            if (tecla==3)
            {
                if (wrReg)
                    menu_sts=30;
                else 
                    menu_sts=6;
            }
            if (tecla==2)
                menu_sts=14;
            if (tecla==1)
            {
                wrReg=true;
                if (semConf.ret_T1==60)
                    semConf.ret_T1=0;
                else
                    semConf.ret_T1++;
            }
            break; 
        case 20:
            if (wrReg==false)
            {
                date=semConf.date;
                meses=semConf.meses;
                anios=semConf.anios;
                horas=semConf.horas;
                minutos=semConf.minutos;
            }
            switch (aux)
            {
                case 0:
                    sprintf(lcd_linea1,"Fecha:[%02u]/%02u/%02u",date,meses,anios);
                    sprintf(lcd_linea2,"Hora:  %02d:%02d     ",horas,minutos);
                    break;
                case 1:
                    sprintf(lcd_linea1,"Fecha:%02u/[%02u]/%02u",date,meses,anios);
                    sprintf(lcd_linea2,"Hora:  %02d:%02d     ",horas,minutos);
                    break;
                case 2:
                    sprintf(lcd_linea1,"Fecha:%02u/%02u/[%02u]",date,meses,anios);
                    sprintf(lcd_linea2,"Hora:  %02d:%02d     ",horas,minutos);
                    break;
                case 3:
                    sprintf(lcd_linea1,"Fecha: %02u/%02u/%02u ",date,meses,anios);
                    sprintf(lcd_linea2,"Hora: [%02d]:%02d     ",horas,minutos);
                    break;
                case 4:
                    sprintf(lcd_linea1,"Fecha: %02u/%02u/%02u ",date,meses,anios);
                    sprintf(lcd_linea2,"Hora:  %02d:[%02d]    ",horas,minutos);
                    break;
                case 5:
                    aux=0;
                    break;
            }                    
            if (tecla==3)
            {
                if (wrReg)
                    menu_sts=31;
                else 
                    menu_sts=7;
            }
            if (tecla==2)
                aux++;
            if (tecla==1)
            {
                switch(aux)
                {
                    case 0: 
                        date++;
                        if (date>=32)
                            date=1;
                        semConf.date=date;
                        break;
                    case 1: 
                        meses++; 
                        if (meses>=13)
                            meses=1;
                        semConf.meses=meses; 
                        break;
                    case 2: 
                        anios++; 
                        if (anios>=30)
                            anios=14;
                        semConf.anios=anios;
                        break;
                    case 3:
                        horas++;
                        if (horas>=24)
                            horas=0;
                        semConf.horas=horas;
                        break;
                    case 4:
                        minutos++;
                        if (minutos>=60)
                            minutos=0;
                        semConf.minutos=minutos;
                        break;
                }
                wrReg=true;
                //t->cambiar=true;
            }
            break;
        case 21:
            switch (aux1)
            {
                case 0:
                    sprintf(lcd_linea1,"INICIO:  [%02d:00]",semConf.IntN_ini);
                    sprintf(lcd_linea2,"FIN:      %02d:00 ",semConf.IntN_fin);
                    break;
                case 1:
                    sprintf(lcd_linea1,"INICIO:   %02d:00 ",semConf.IntN_ini);
                    sprintf(lcd_linea2,"FIN:     [%02d:00]",semConf.IntN_fin);
                    break;
                case 2:
                    aux1=0;
                    break;
            }
            if (tecla==3)
            {
                if (wrReg)
                    menu_sts=30;
                else 
                    menu_sts=8;
            }
            if (tecla==2)
                aux1++;
            if (tecla==1)
            {
                wrReg=true;
                switch (aux1)
                {
                    case 0:
                        if (semConf.IntN_ini==23)
                            semConf.IntN_ini=0;
                        else
                            semConf.IntN_ini++;
                        break;
                    case 1:
                        if (semConf.IntN_fin==23)
                            semConf.IntN_fin=0;
                        else
                            semConf.IntN_fin++;
                        break;
                }
            }
            break;
        case 22:
            switch (aux1)
            {
                case 0:
                    sprintf(lcd_linea1,"INICIO:  [%02d:00]",semConf.Peat_ini);
                    sprintf(lcd_linea2,"FIN:      %02d:00 ",semConf.Peat_fin);
                    break;
                case 1:
                    sprintf(lcd_linea1,"INICIO:   %02d:00 ",semConf.Peat_ini);
                    sprintf(lcd_linea2,"FIN:     [%02d:00]",semConf.Peat_fin);
                    break;
                case 2:
                    aux1=0;
                    break;
            }
            if (tecla==3)
            {
                if (wrReg)
                    menu_sts=30;
                else 
                    menu_sts=9;
            }
            if (tecla==2)
                aux1++;
            if (tecla==1)
            {
                wrReg=true;
                switch (aux1)
                {
                    case 0:
                        if (semConf.Peat_ini==24)
                            semConf.Peat_ini=0;
                        else
                            semConf.Peat_ini++;
                        break;
                    case 1:
                        if (semConf.Peat_fin==24)
                            semConf.Peat_fin=0;
                        else
                            semConf.Peat_fin++;
                        break;
                }
            }
            break;
        case 23:
            
            if (!wrReg)
            {
                add=readLastAddEv();   
            }
            readEvent(add);
            sprintf(lcd_linea1,"%02d/%02d %02d:%02d:%02d",semConf.ev_dia,semConf.ev_mes,semConf.ev_hor,semConf.ev_min,semConf.ev_seg);
            sprintf(lcd_linea2,"%s",Eventos[semConf.codigo][0]);
            //sprintf(lcd_linea2,"%d      ",add);
            if (tecla==3)
            {
                wrReg=false;
                menu_sts=10;
            }
            if (tecla==1)
            {
                wrReg=true;
                add=add-8;
                if (add<28)
                    add=28;
            }
            break;
        case 24:
            sprintf(lcd_linea1,"GPS: %c  %d  %d     ", GPS_getValid(),GPS_getStatus(),GPS_getReint());
            sprintf(lcd_linea2,"[T1]ref [T3]sal ");
            if (tecla==3)
                menu_sts=11;
            if (tecla==1)
            {
                GPS_initTask();
                menu_sts=11;
            }
            break;
        case 25:
            sprintf(lcd_linea1,"BORRAR EEPROM.? ");
            sprintf(lcd_linea2,"  Pres.Enter[T3]");
            if (tecla==3)
                menu_sts=26;
            if (tecla==2)
                menu_sts=27; // ultima era 12
            break;
        case 26:
            sprintf(lcd_linea1,"REINIC. EEPROM.?");
            sprintf(lcd_linea2,"[T1]SI    [T2]NO");
            if (tecla==2)
                menu_sts=25;
            if (tecla==1)
            {
                initEeprom();
                readConf();
                menu_sts=4;
            }
            break; 
        case 27:
            sprintf(lcd_linea1,"ANALOG:         ");
            sprintf(lcd_linea2,"[%d] [%d] %d   ",semConf.V_linea,semConf.ret1,getAnalogTaskSt());
            if (tecla==3)
                menu_sts=12;
            if (tecla==2)
                menu_sts=12; // ultima era 12
            break;
        case 30:
            sprintf(lcd_linea1,"CAMBIAR CONFIG.?");
            sprintf(lcd_linea2,"[T1]SI    [T2]NO");
            wrReg=false;
            if (tecla==2)
            {
                sprintf(lcd_linea1,"CAMBIAR CONFIG. ");
                sprintf(lcd_linea2,"          [T2]NO");
                readConf();
                
                menu_sts=4;
            }
            if (tecla==1)
            {
                sprintf(lcd_linea1,"CAMBIAR CONFIG. ");
                sprintf(lcd_linea2,"[T1]SI          ");
                writeConf();
                 
                menu_sts=4;
            }
            break;
        case 31:
            sprintf(lcd_linea1,"CAMBIAR CONFIG.?");
            sprintf(lcd_linea2,"[T1]SI    [T2]NO");
            wrReg=false;
            if (tecla==2)
                menu_sts=4;
            if (tecla==1)
            {
                semConf.date=date;
                semConf.meses=meses;
                semConf.anios=anios;
                semConf.horas=horas;
                semConf.minutos=minutos;
                semConf.actualizar=1;
                //BQ32k_set_DateTime();
                
                menu_sts=4;
            }
            break;
        default:
            break;
    }
    vGotoxyLCD(1,1);    vPuts_LCD(lcd_linea1);
    vGotoxyLCD(1,2);    vPuts_LCD(lcd_linea2);
    tecla=0;
    /*
    if (tecla>=1 && tecla<=3  )
        tecla=0;
    if (actTimeDate)
    {
        BQ32k_get_date(t);
        BQ32k_get_time(t);
        actTimeDate=false;
    }
    */
    return (menu_sts);
}

