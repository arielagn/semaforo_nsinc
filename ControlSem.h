/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef CONTROLSEM_H
#define	CONTROLSEM_H

#include <xc.h> // include processor files - each processor file is guarded.  

// TODO Insert appropriate #include <>

// TODO Insert C++ class definitions if appropriate

// TODO Insert declarations
typedef enum {
    DOS_T       =2,
    TRES_T      =3,
    CUATRO_T    =4,
    DOS_T_P     =5,
    TRES_T_P    =6,
    UN_T_P      =7   
}SEM_MODO;
typedef struct {
    // Campos de configuracion
   uint8_t      IntN_ini;   //pos 2
   uint8_t      IntN_fin;   //pos 3
   uint8_t      Peat_ini;   //pos 9
   uint8_t      Peat_fin;   //pos 12
   uint8_t      TV1;        //pos 4
   uint8_t      TV2;        //pos 5
   uint8_t      TV3;        //pos 6
   uint8_t      TV4;        //pos 7
   uint8_t      T_amar;     //pos 8
   uint8_t      ret_T1;     //pos 13
   SEM_MODO     modo;       //pos 11
   uint8_t      IntF;       //pos 10
   // Campos utiles para sincronismo
   uint32_t segDia;
   uint16_t ciclo;
   uint16_t restoInc;
   uint16_t restoDec;
   // Campos del RTC Reloj verdadero del semaforo
   uint32_t      segundos;
   uint32_t      minutos;
   uint32_t      horas;
   uint8_t      dia;
   uint8_t      date;
   uint8_t      meses;
   uint8_t      anios;
   uint8_t      actualizar;
   // Campo para eventos
   uint8_t codigo;
   uint16_t     Reg;    //pos 26 y 27
   uint8_t ev_seg;
   uint8_t ev_min;
   uint8_t ev_hor;
   uint8_t ev_dia;
   uint8_t ev_mes;
   uint8_t ev_anio;
   // campos analogico
   uint16_t V_linea;
   uint16_t ret1;
   uint16_t ret2;
   uint16_t ret3;
   uint16_t ret4;
   
}Sem_Conf;



// Comment a function and leverage automatic documentation with slash star star
/**
    <p><b>Function prototype:</b></p>
  
    <p><b>Summary:</b></p>

    <p><b>Description:</b></p>

    <p><b>Precondition:</b></p>

    <p><b>Parameters:</b></p>

    <p><b>Returns:</b></p>

    <p><b>Example:</b></p>
    <code>
 
    </code>

    <p><b>Remarks:</b></p>
 */
// TODO Insert declarations or function prototypes (right here) to leverage 
// live documentation
void sem_Estado();
void sem_Sincro();
void sem_Control();
uint8_t sem_task();
void intSemSeg();
#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

