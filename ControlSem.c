/*
 * File:   ControlSem.c
 * Author: LPT13
 *
 * Created on November 23, 2017, 8:49 PM
 */

#include <stdbool.h>
#include "xc.h"
#include "ControlSem.h"
#include "Bq32k.h"
#include "PCA9555.h"
#include "ServicioTiempo.h"
#include "A24LC512.h"
#include "mcc_generated_files/tmr2.h"

static bool semSeg;
Sem_Conf semConf;
//Array Secuencia de luces 4 Sem 
uint16_t const S_4[16]={0x2490,0x2498,0x2484,0x2488,0x2490,0x24D0,0x2430,0x2450,
                        0x2490,0x2690,0x2190,0x2290,0x2490,0x3490,0x0C90,0x1490};
uint16_t const P_4[3]={0x2490,0x0C90,0x2490};

void intSemSeg()
{
    semSeg=true;
}
/* Tarea que realiza los cambio de modo del semaforo (intermitencia, peatonal, etc
 
 */
void sem_Estado()
{ 
    switch (semConf.IntF)
    {
        case 0:     //Sin estado desde el inicio, arranque de sincronismo  
            //if (GPS_getStatus()==5 && GPS_getValid()=='A') //  La rutuna de GPS termina (si no termian es porque no esta GPS o se quemo)
            semConf.IntF++;   // Todo bien con el GPS listo para sincronizar
        case 1:
            
            if (semConf.restoDec==1)
            {
                if (semConf.modo==2 || semConf.modo==3 || semConf.modo==4) semConf.IntF=2;
                if (semConf.modo==5 || semConf.modo==6) semConf.IntF=3;
                /*    
                {   if (semConf.Peat_ini==semConf.Peat_fin) semConf.IntF=3; //si es con peatonal y los tiempos son iguales, va a peatona
                    else if (semConf.Peat_ini!=semConf.Peat_fin && semConf.Peat_ini<semConf.Peat_fin && (semConf.horas>=semConf.Peat_ini && semConf.horas<semConf.Peat_fin)) semConf.IntF=3; //si son distintos y se da la hora a peatonal
                    else if (semConf.Peat_ini!=semConf.Peat_fin && semConf.Peat_ini>semConf.Peat_fin && (semConf.horas>=semConf.Peat_ini || semConf.horas<semConf.Peat_fin)) semConf.IntF=3;
                    else semConf.IntF=2; // sino va a normal sin peatonal
                }
                if (semConf.modo==7) semConf.IntF=3;
                */
                if (semConf.IntN_ini!=semConf.IntN_fin && semConf.IntN_ini<semConf.IntN_fin && (semConf.horas>=semConf.IntN_ini && semConf.horas<semConf.IntN_fin)) semConf.IntF=5; //Paso a intermitente nocturno
                if (semConf.IntN_ini!=semConf.IntN_fin && semConf.IntN_ini>semConf.IntN_fin && (semConf.horas>=semConf.IntN_ini || semConf.horas<semConf.IntN_fin)) semConf.IntF=5;
            }
            break;
        case 2:
            if (semConf.modo==2 || semConf.modo==3 || semConf.modo==4) semConf.IntF=2;
            if (semConf.modo==5 || semConf.modo==6) semConf.IntF=3;
            /*
            {   if (semConf.Peat_ini==semConf.Peat_fin) semConf.IntF=3; //si es con peatonal y los tiempos son iguales, va a peatona
                else if (semConf.Peat_ini!=semConf.Peat_fin && semConf.Peat_ini<semConf.Peat_fin && (semConf.horas>=semConf.Peat_ini && semConf.horas<semConf.Peat_fin)) semConf.IntF=3; //si son distintos y se da la hora a peatonal
                else if (semConf.Peat_ini!=semConf.Peat_fin && semConf.Peat_ini>semConf.Peat_fin && (semConf.horas>=semConf.Peat_ini || semConf.horas<semConf.Peat_fin)) semConf.IntF=3;
                else semConf.IntF=2; // sino va a normal sin peatonal
            }
            */
            if (semConf.IntN_ini!=semConf.IntN_fin && semConf.IntN_ini<semConf.IntN_fin && (semConf.horas>=semConf.IntN_ini && semConf.horas<semConf.IntN_fin)) semConf.IntF=5; //Paso a intermitente nocturno
            if (semConf.IntN_ini!=semConf.IntN_fin && semConf.IntN_ini>semConf.IntN_fin && (semConf.horas>=semConf.Peat_ini || semConf.horas<semConf.Peat_fin)) semConf.IntF=5;
            break;
        case 3:
            if (semConf.modo==2 || semConf.modo==3 || semConf.modo==4) semConf.IntF=2;
            if (semConf.modo==5 || semConf.modo==6) semConf.IntF=3;
            /*
            {   if (semConf.Peat_ini==semConf.Peat_fin) semConf.IntF=3; //si es con peatonal y los tiempos son iguales, va a peatona
                else if (semConf.Peat_ini!=semConf.Peat_fin && semConf.Peat_ini<semConf.Peat_fin && (semConf.horas>=semConf.Peat_ini && semConf.horas<semConf.Peat_fin)) semConf.IntF=3; //si son distintos y se da la hora a peatonal
                else if (semConf.Peat_ini!=semConf.Peat_fin && semConf.Peat_ini>semConf.Peat_fin && (semConf.horas>=semConf.Peat_ini || semConf.horas<semConf.Peat_fin)) semConf.IntF=3;
                else semConf.IntF=0; // sino va a normal sin peatonal
            }
            */
            if (semConf.IntN_ini!=semConf.IntN_fin && semConf.IntN_ini<semConf.IntN_fin && (semConf.horas>=semConf.IntN_ini && semConf.horas<semConf.IntN_fin)) semConf.IntF=5; //Paso a intermitente nocturno
            if (semConf.IntN_ini!=semConf.IntN_fin && semConf.IntN_ini>semConf.IntN_fin && (semConf.horas>=semConf.Peat_ini || semConf.horas<semConf.Peat_fin)) semConf.IntF=5;
            break;
        case 4:
            
            break;
        case 5:
            if (semConf.horas==semConf.IntN_fin) semConf.IntF=0;
            break;
        case 6:
            if (semConf.V_linea>=570) //salgo de modo baja tension si se eleva la tension. 
            {
                semConf.IntF=0;
            }
        default:
            break;
        
    }
    //Sincro diario e inicio de GPS  
    if (semConf.segDia==1) //todos los dias a las 0 hs sincroniza y toma GPS
    {
        if(GPS_getStatus()==0 || GPS_getStatus()==5)
        {
            GPS_initTask(); // Inicializo GPS
            if (semConf.IntF!=4)
                semConf.IntF=0; //Paso a estado 0 de sincronizacion
        }
        
    }
    // Reintento de captacion satelites 
    if (GPS_getValid()!='A' && semConf.segundos==10)
        GPS_initTask();
        
    // Me fijo si hay baja tension. Valor a introducir tal que todavia funcione el cpu
    if (semConf.IntF<6 && semConf.V_linea<=540)
    {
        semConf.IntF=6; // Modo baja tension
        writeEvent(6); // Gravo evento,baja tension
    }
}
/*  Tarea que actualiza el estado del sincronismo del semaforo
 */
void sem_Sincro()
{
    
    semConf.segDia=semConf.horas*3600+semConf.minutos*60+semConf.segundos;
    switch(semConf.modo)
    {
        case 2:
            semConf.ciclo=semConf.TV1+semConf.T_amar+1+semConf.T_amar+semConf.TV2+semConf.T_amar+1+semConf.T_amar;
            break;
        case 3:
            semConf.ciclo=semConf.TV1+semConf.T_amar+1+semConf.T_amar+semConf.TV2+semConf.T_amar+1+semConf.T_amar+
                    +semConf.TV3+semConf.T_amar+1+semConf.T_amar;
            break;
        case 4:
            semConf.ciclo=semConf.TV1+semConf.T_amar+1+semConf.T_amar+semConf.TV2+semConf.T_amar+1+semConf.T_amar+
                    +semConf.TV3+semConf.T_amar+1+semConf.T_amar+semConf.TV4+semConf.T_amar+1+semConf.T_amar;
            break;
        case 5:
            semConf.ciclo=semConf.TV1+semConf.T_amar+1+semConf.T_amar+semConf.TV2+semConf.T_amar+1+semConf.T_amar+
                    +semConf.TV4+semConf.T_amar+1+semConf.T_amar;
            break;
        case 6:
            semConf.ciclo=semConf.TV1+semConf.T_amar+1+semConf.T_amar+semConf.TV2+semConf.T_amar+1+semConf.T_amar+
                    +semConf.TV3+semConf.T_amar+1+semConf.T_amar+semConf.TV4+semConf.T_amar+1+semConf.T_amar;
            break;
        case 7:
            break;
        default:
            break;
    }
    semConf.restoDec=semConf.ciclo-((semConf.segDia-semConf.ret_T1)%semConf.ciclo);
    semConf.restoInc=(semConf.segDia-semConf.ret_T1)%semConf.ciclo;
}
/*  Tarea de control de movimientos del semaforo
 */
void sem_Control()
{
    uint8_t paso1,paso2,paso3,paso4,paso5,paso6,paso7,paso8,paso9,paso10;
    uint8_t paso11,paso12,paso13,paso14,paso15,paso16,paso17,paso18,paso19,paso20;
    static bool lamp;
    uint8_t    port0,port1;
    uint16_t    temp;
    switch (semConf.IntF)
    {
        case 0:
        case 1:
        case 4:
        case 5:
        case 6:
            if (lamp)
            {
                i9555_write(0,0b01001000);         //Todos los semaforos en amarillo
                i9555_write(1,0b00010010);
            }
            else
            {
                i9555_write(0,0b00000000);         //Todos los semaforos apagados
                i9555_write(1,0b00000000);
            }
            break;
        case 2: // Normal sin peatonal
            paso1=1;
            paso2=1+semConf.T_amar;
            paso3=paso2+semConf.TV1-6;
            paso4=paso2+semConf.TV1;
            paso5=paso4+semConf.T_amar;
            paso6=paso5+1;
            paso7=paso6+semConf.T_amar;
            paso8=paso7+semConf.TV2-6;
            paso9=paso7+semConf.TV2;
            paso10=paso9+semConf.T_amar;
            paso11=paso10+1;
            paso12=paso11+semConf.T_amar;
            paso13=paso12+semConf.TV3-6;
            paso14=paso12+semConf.TV3;
            paso15=paso14+semConf.T_amar;
            paso16=paso15+1;
            paso17=paso16+semConf.T_amar;
            paso18=paso17+semConf.TV4-6;
            paso19=paso17+semConf.TV4;
            paso20=paso19+semConf.T_amar;
            if (paso1>=semConf.restoInc)
            {
                port0=S_4[0];             //Cargo el valor del vector
                temp=S_4[0];
                port1=temp>>8;  
            }
            else if (paso2>=semConf.restoInc)
            {
                port0=S_4[1];             //Cargo el valor del vector
                temp=S_4[1];
                port1=temp>>8;
            }
            else if(paso3>=semConf.restoInc)
            {
                port0=S_4[2];             //Cargo el valor del vector
                temp=S_4[2];
                port1=temp>>8;  
            }
            else if(paso4>=semConf.restoInc)
            {
                if (lamp)
                {
                    port0=S_4[2]&0xDB;             //Cargo el valor del vector
                    temp=S_4[2];
                    port1=(temp>>8)&0x36;  
                }
                else
                {
                    port0=S_4[2];             //Cargo el valor del vector
                    temp=S_4[2];
                    port1=(temp>>8);  
                }  
            }
            else if(paso5>=semConf.restoInc)
            {
                port0=S_4[3];             //Cargo el valor del vector
                temp=S_4[3];
                port1=temp>>8;  
            }
            else if(paso6>=semConf.restoInc)
            {
                port0=S_4[4];             //Cargo el valor del vector
                temp=S_4[4];
                port1=temp>>8;  
            }
            else if(paso7>=semConf.restoInc)
            {
                port0=S_4[5];             //Cargo el valor del vector
                temp=S_4[5];
                port1=(temp>>8);    
            }
            else if(paso8>=semConf.restoInc)
            {
                port0=S_4[6];             //Cargo el valor del vector
                temp=S_4[6];
                port1=(temp>>8); 
            }
            else if(paso9>=semConf.restoInc)
            {
                if (lamp)
                {
                    port0=S_4[6]&0xDB;             //Cargo el valor del vector
                    temp=S_4[6];
                    port1=(temp>>8)&0x36;  
                }
                else
                {
                    port0=S_4[6];             //Cargo el valor del vector
                    temp=S_4[6];
                    port1=(temp>>8);  
                }
            }
            else if(paso10>=semConf.restoInc)
            {
                port0=S_4[7];             //Cargo el valor del vector
                temp=S_4[7];
                port1=(temp>>8);
            }
            else if(paso11>=semConf.restoInc)
            {
                port0=S_4[8];             //Cargo el valor del vector
                temp=S_4[8];
                port1=(temp>>8);
            }
            else if (paso12>=semConf.restoInc)
            {
                port0=S_4[9];             //Cargo el valor del vector
                temp=S_4[9];
                port1=(temp>>8);
            }
            else if(paso13>=semConf.restoInc)
            {
                port0=S_4[10];             //Cargo el valor del vector
                temp=S_4[10];
                port1=(temp>>8);
            }
            else if(paso14>=semConf.restoInc)
            {
                if (lamp)
                {
                    port0=S_4[10]&0xDB;             //Cargo el valor del vector
                    temp=S_4[10];
                    port1=(temp>>8)&0x36;  
                }
                else
                {
                    port0=S_4[10];             //Cargo el valor del vector
                    temp=S_4[10];
                    port1=(temp>>8);  
                }
            }
            else if(paso15>=semConf.restoInc)
            {
                port0=S_4[11];             //Cargo el valor del vector
                temp=S_4[11];
                port1=(temp>>8);
            }
            else if(paso16>=semConf.restoInc)
            {
                port0=S_4[12];             //Cargo el valor del vector
                temp=S_4[12];
                port1=(temp>>8);
            }
            else if(paso17>=semConf.restoInc)
            {
                port0=S_4[13];             //Cargo el valor del vector
                temp=S_4[13];
                port1=(temp>>8);
            }
            else if(paso18>=semConf.restoInc)
            {
                port0=S_4[14];             //Cargo el valor del vector
                temp=S_4[14];
                port1=(temp>>8);
            }
            else if(paso19>=semConf.restoInc)
            {
                if (lamp)
                {
                    port0=S_4[14]&0xDB;             //Cargo el valor del vector
                    temp=S_4[14];
                    port1=(temp>>8)&0x36;  
                }
                else
                {
                    port0=S_4[14];             //Cargo el valor del vector
                    temp=S_4[14];
                    port1=(temp>>8);  
                }
            }
            else if(paso20>=semConf.restoInc)
            {
                port0=S_4[15];             //Cargo el valor del vector
                temp=S_4[15];
                port1=(temp>>8);
            }
            i9555_write(0,port0);         //Y lo saco por el I/O
            i9555_write(1,port1);         //Y lo saco por el I/O
            break;
        case 3: // Normal con peatonal
            paso1=1;
            paso2=1+semConf.T_amar;
            paso3=paso2+semConf.TV1-6;
            paso4=paso2+semConf.TV1;
            paso5=paso4+semConf.T_amar;
            paso6=paso5+1;
            paso7=paso6+semConf.T_amar;
            paso8=paso7+semConf.TV2-6;
            paso9=paso7+semConf.TV2;
            paso10=paso9+semConf.T_amar;
            if (semConf.modo==5) //dos tiempos + peatonal
            {
                paso11=paso10+1;            //dif
                paso12=paso11+semConf.TV4-6; 
                paso13=paso11+semConf.TV4;    // intermitente
                paso14=paso13+semConf.T_amar; // intermitente
                paso15=paso14+semConf.T_amar; // intermitente
                paso16=paso15+1;        // No cuenta
                paso17=paso16+semConf.TV4-6;  
                paso18=paso16+semConf.TV4;    
                paso19=paso18+semConf.T_amar;  
                paso20=paso19+semConf.T_amar; 
            }
            else 
            {
                paso11=paso10+1;
                paso12=paso11+semConf.T_amar;
                paso13=paso12+semConf.TV3-6;
                paso14=paso12+semConf.TV3;
                paso15=paso14+semConf.T_amar;
                paso16=paso15+1;
                paso17=paso16+semConf.TV4-6;  //dif
                paso18=paso16+semConf.TV4;    // intermitente
                paso19=paso18+semConf.T_amar;  // intermitente
                paso20=paso19+semConf.T_amar; // intermitente
            }
            if (paso1>=semConf.restoInc)
            {
                port0=S_4[0];             //Cargo el valor del vector
                temp=S_4[0];
                port1=temp>>8;  
            }
            else if (paso2>=semConf.restoInc)
            {
                port0=S_4[1];             //Cargo el valor del vector
                temp=S_4[1];
                port1=temp>>8;
            }
            else if(paso3>=semConf.restoInc)
            {
                port0=S_4[2];             //Cargo el valor del vector
                temp=S_4[2];
                port1=temp>>8;  
            }
            else if(paso4>=semConf.restoInc)
            {
                if (lamp)
                {
                    port0=S_4[2]&0xDB;             //Cargo el valor del vector
                    temp=S_4[2];
                    port1=(temp>>8)&0x36;  
                }
                else
                {
                    port0=S_4[2];             //Cargo el valor del vector
                    temp=S_4[2];
                    port1=(temp>>8);  
                }  
            }
            else if(paso5>=semConf.restoInc)
            {
                port0=S_4[3];             //Cargo el valor del vector
                temp=S_4[3];
                port1=temp>>8;  
            }
            else if(paso6>=semConf.restoInc)
            {
                port0=S_4[4];             //Cargo el valor del vector
                temp=S_4[4];
                port1=temp>>8;  
            }
            else if(paso7>=semConf.restoInc)
            {
                port0=S_4[5];             //Cargo el valor del vector
                temp=S_4[5];
                port1=(temp>>8);    
            }
            else if(paso8>=semConf.restoInc)
            {
                port0=S_4[6];             //Cargo el valor del vector
                temp=S_4[6];
                port1=(temp>>8); 
            }
            else if(paso9>=semConf.restoInc)
            {
                if (lamp)
                {
                    port0=S_4[6]&0xDB;             //Cargo el valor del vector
                    temp=S_4[6];
                    port1=(temp>>8)&0x36;  
                }
                else
                {
                    port0=S_4[6];             //Cargo el valor del vector
                    temp=S_4[6];
                    port1=(temp>>8);  
                }
            }
            else if(paso10>=semConf.restoInc)
            {
                port0=S_4[7];             //Cargo el valor del vector
                temp=S_4[7];
                port1=(temp>>8);
            }
            else if(paso11>=semConf.restoInc && semConf.modo==6)
            {
                port0=S_4[8];             //Cargo el valor del vector
                temp=S_4[8];
                port1=(temp>>8);
            }
            else if(paso11>=semConf.restoInc && semConf.modo==5)
            {
                port0=P_4[0];             //Cargo el valor del vector
                temp=P_4[0];
                port1=(temp>>8);
            }
            else if (paso12>=semConf.restoInc && semConf.modo==6)
            {
                port0=S_4[9];             //Cargo el valor del vector
                temp=S_4[9];
                port1=(temp>>8);
            }
            else if(paso12>=semConf.restoInc && semConf.modo==5)
            {
                port0=P_4[1];             //Cargo el valor del vector
                temp=P_4[1];
                port1=(temp>>8);
            }
            else if(paso13>=semConf.restoInc && semConf.modo==6)
            {
                port0=S_4[10];             //Cargo el valor del vector
                temp=S_4[10];
                port1=(temp>>8);
            }
            else if(paso13>=semConf.restoInc && semConf.modo==5)
            {
                if (lamp)
                {
                    port0=P_4[1]&0xDB;             //Cargo el valor del vector
                    temp=P_4[1];
                    port1=(temp>>8)&0x36;  
                }
                else
                {
                    port0=P_4[1];             //Cargo el valor del vector
                    temp=P_4[1];
                    port1=(temp>>8);  
                }
            }
            else if(paso14>=semConf.restoInc && semConf.modo==6)
            {
                if (lamp)
                {
                    port0=S_4[10]&0xDB;             //Cargo el valor del vector
                    temp=S_4[10];
                    port1=(temp>>8)&0x36;  
                }
                else
                {
                    port0=S_4[10];             //Cargo el valor del vector
                    temp=S_4[10];
                    port1=(temp>>8);  
                }
            }
            else if(paso14>=semConf.restoInc && semConf.modo==5)
            {
                if (lamp)
                {
                    port0=P_4[2];             //Cargo el valor del vector
                    temp=P_4[2];
                    port1=(temp>>8)&0x1F;  
                }
                else
                {
                    port0=P_4[2];             //Cargo el valor del vector
                    temp=P_4[2];
                    port1=(temp>>8);  
                }
            }
            else if(paso15>=semConf.restoInc && semConf.modo==6)
            {
                port0=S_4[11];             //Cargo el valor del vector
                temp=S_4[11];
                port1=(temp>>8);
            }
            else if(paso15>=semConf.restoInc && semConf.modo==5)
            {
                if (lamp)
                {
                    port0=P_4[2];             //Cargo el valor del vector
                    temp=P_4[2];
                    port1=(temp>>8)&0x1F;  
                }
                else
                {
                    port0=P_4[2];             //Cargo el valor del vector
                    temp=P_4[2];
                    port1=(temp>>8);  
                }
            }
            else if(paso16>=semConf.restoInc)
            {
                port0=P_4[0];             //Cargo el valor del vector
                temp=P_4[0];
                port1=(temp>>8);
            }
            else if(paso17>=semConf.restoInc)
            {
                port0=P_4[1];             //Cargo el valor del vector
                temp=P_4[1];
                port1=(temp>>8);
            }
            else if(paso18>=semConf.restoInc)
            {
                if (lamp)
                {
                    port0=P_4[1]&0xDB;             //Cargo el valor del vector
                    temp=P_4[1];
                    port1=(temp>>8)&0x36;  
                }
                else
                {
                    port0=P_4[1];             //Cargo el valor del vector
                    temp=P_4[1];
                    port1=(temp>>8);  
                }
            }
            else if(paso19>=semConf.restoInc)
            {
                if (lamp)
                {
                    port0=P_4[2];             //Cargo el valor del vector
                    temp=P_4[2];
                    port1=(temp>>8)&0x1F;  
                }
                else
                {
                    port0=P_4[2];             //Cargo el valor del vector
                    temp=P_4[2];
                    port1=(temp>>8);  
                }
            }
            else if(paso20>=semConf.restoInc)
            {
                if (lamp)
                {
                    port0=P_4[2];             //Cargo el valor del vector
                    temp=P_4[2];
                    port1=(temp>>8)&0x1F;  
                }
                else
                {
                    port0=P_4[2];             //Cargo el valor del vector
                    temp=P_4[2];
                    port1=(temp>>8);  
                }
            }
            i9555_write(0,port0);         //Y lo saco por el I/O
            i9555_write(1,port1);         //Y lo saco por el I/O
            break;
        default:
            break;
            
            
    }
    lamp=!lamp;
}

/*      Tarea general del semaforo. Coordina a las tareas anteriores
 */
uint8_t sem_task()
{
    //if (GPS_getStatus()>0 && GPS_getStatus()<5)
    //    return (0);      // Salgo si el gps esta actualizando. GPS necesita tiempo de ejecucion
    if (TMR2_GetElapsedThenClear()) //(semSeg) 
    {
        sem_Sincro();
        sem_Estado();
        sem_Control();
        semSeg=false;
    }
    return (1);
}