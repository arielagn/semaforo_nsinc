/*
 * File:   supervisor.c
 * Author: LPT13
 *
 * Created on January 18, 2018, 7:28 PM
 */


#include "xc.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/tmr3.h"
#include "Bq32k.h"
#include "A24LC512.h"
#include "supervisor.h"
static uint16_t sup_count;

void supervisor()
{
       
    
    if (TMR3_GetElapsedThenClear())
    {   SUP_OUT_Toggle();
        if (sup_count<40)
        {
            sup_count++;
            
        }
        else
        {
            sup_count=0;
            //writeEvent(16);
            //SUP_OUT_SetHigh();
            //TMR3_Stop();
        }
    }
    
}

void supInPin(void)
{
    
    sup_count=0;
    
}
